import React from 'react';
import theme from '../config/theme';
import CustomImage from './common/customimage';
import CustomImageWithoutLazyLoad from './common/customimagewithoutlazylaod'
import Slider from 'react-slick'
import GoButton from '../components/common/gobutton'

export default React.memo(function BolgSoloCard(props) {
    const settings = {
        dots: true,
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        lazyLoad: true,
        responsive: [
            {
                breakpoint: 9999,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 959,
                slidesToShow: 4,
                slidesToScroll: 1,
            }
        ]
    }
    return (
        <article className="maincontainer">
            <h2 className="headMobile">{props.title}</h2>
            <div className="imagecontainer">
                <CustomImage image={props.mainimg.url} alt={props.mainimg.alt} href={props.mainimg.href} mobilewidth={270}
                    mobileheight={164} tabletwidth={295} tabletheight={596} desktopwidth={320} desktopheight={596} largedesktopwidth={380}
                    largedesktopheight={800} />
            </div>
            <div className="detailcontainer">
                <h2 className="headdesktop">{props.title}</h2>
                <p className='Deskdescription'>{props.description}</p>
                <div className='gobutton'>
                    <GoButton title='View More' />
                </div>
                <div className="smallimageholder">
                    <Slider {...settings}>
                        {props.imgs.map((item, index) =>
                            <div className='sliderimage' key={index}>
                                <CustomImageWithoutLazyLoad image={item.url} alt={item.alt} mobilewidth={81}
                                    mobileheight={70} tabletwidth={295} tabletheight={596} desktopwidth={320} desktopheight={596} largedesktopwidth={380}
                                    largedesktopheight={800} />
                            </div>

                        )}
                    </Slider>
                </div>
                <p className='Mobdescription'>{props.description}</p>
            </div>

            <style jsx>{`
                .headMobile{
                    font-size: 1.5em;
                    font-weight: 800;
                    margin-bottom: 25px;
                }   
                .gobutton {
                    display: none;
                }
                .headdesktop,.Deskdescription{display:none;} 
                img{
                    width:23%;
                    height:70px;
                }
                .Mobdescription{
                    margin:20px 0;
                    color:${theme.colors.darkgray};
                }
                p{
                    line-height:1.5;
                    font-size: ${theme.fontsize.smallerFont};
                }
                .sliderimage { 
                    width: 93% !important; 
                    height: 67px;
                }
                .smallimageholder { 
                    margin-right: -2%; 
                    margin-top: 4px;
                }

                @media screen and (min-width: 680px) {
                    .sliderimage { height: 100px; }
                }
              @media screen and (min-width:960px){
                .headMobile,.Mobdescription{display:none}
                .headdesktop,.Deskdescription{
                    display:block;
                    margin-bottom: 25px;
                } 
                .maincontainer{
                    display:flex;
                    align-items: center;
                    justify-content:space-between;
                }
                .imagecontainer{
                    width:50%;
                    height: 400px;
                }
                .detailcontainer{
                    width: 43%;
                    display: flex;
                    flex-direction: column;
                    justify-content: flex-end;
                }
                .gobutton {
                    display: inline-block;
                    margin-bottom: 25px;
                }
              }
              @media screen and (min-width: 1280px) {
                .detailcontainer{
                    width: 45%;
                }
              }
              @media screen and (min-width: 1680px) {
                .imagecontainer{ height: 550px;}
              }
             `}</style>
        </article>
    )
})