import React from 'react'
import PageHeader from '../components/common/pageheader';
import sitesettings from '../config/sitesettings'
import fetch from 'isomorphic-unfetch'
import dynamic from "next/dynamic"
import theme from '../config/theme';
import Slider from 'react-slick'
import GoButton from '../components/common/gobutton';
import PageFooter from '../components/common/pagefooter';

const CustomImage = dynamic(import("../components/common/customimage"));
const Carousel1 = dynamic(import("../components/carousel1"));
const RectangleCategory1 = dynamic(import("../components/rectanglecategory1"));
const BlogHorizontal = dynamic(import("../components/BlogHorizontal"));
const BlogListLarge1 = dynamic(import("../components/bloglistlarge1"));
const SliderButtons = dynamic(import("../components/common/sliderbuttons"));
const BlogSolo1 = dynamic(import("../components/blogsolo1"));
const VideoSolo = dynamic(import("../components/videosolo"));


export default class index extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      showModal:"false",
      carouselindex: 0,
      bloghorindex: 0,
      bloglistindex: 0,
      searchinput:'',
      data:[]
    }
    this.myDivSection = React.createRef()
  }

  static async getInitialProps({ req }) {
    const res = await fetch(sitesettings.serviceUrl + '/dev/home', {
      headers: sitesettings.serviceHeader,
    }).catch((error) => {
      console.log(error)
    })

    if (res.status === 200) {
      const json = await res.json();
      return { serverdata: json }
    }
    else {
      console.log("status" + res.status)
      return { serverdata: null }
    }
  }
  
  scrollDown() {
    if (this.myDivSection.current) {
      let toPos = this.myDivSection.current.offsetTop;
      console.log(toPos);
      window.scrollTo(0, toPos - 80)
    }
  }

  next = (type) => {
    if (type === 'carousel') {
      this.carousel.slickNext();
    }
    else if (type === "bloghorizontal") {
      this.bloghorizontal.slickNext();
    }
    else if (type === "bloglistlarge") {
      this.bloglistlarge.slickNext();
    }
  }

  previous = (type) => {
    if (type === 'carousel') {
      this.carousel.slickPrev();
    }
    else if (type === "bloghorizontal") {
      this.bloghorizontal.slickPrev();
    }
    else if (type === "bloglistlarge") {
      this.bloglistlarge.slickPrev();
    }
  }

  hideModal(){
    this.setState({showModal:"false"});
    console.log(this.state.showModal);
  }

  carouselchange = (e, name) => {
    console.log(e)
    this.setState({ [name]: e })
  }

  searchcity = (e) => {
    // this.setState({showModal:"true"});
    let newdata = [];
    let currentdata=[];
    let input = e.target.value;
    if(input !== ""){
      currentdata = sitesettings.searchcities.cities;
      newdata = currentdata.filter((item)=>{
        return item.categories.toUpperCase().includes(input.toUpperCase());
      })
    }  
    else{
      newdata = sitesettings.searchcities.cities;
    }
    this.setState({showModal:"true",searchinput:input,data : newdata })
  }

  render() {
    // console.log(this.props)
    const { serverdata } = this.props;
    const carousel1 = {
      arrows: false,
      afterChange: (e) => this.carouselchange(e, 'carouselindex'),
      responsive: [
        {
          breakpoint: 9999,
          settings: {
            infinite: false,
            speed: 500,
            slidesToShow: 2,
            slidesToScroll: 1,
            lazyLoad: true,

            arrows: false,
          }
        },
        {
          breakpoint: 1365,
          settings: {
            infinite: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: false,
          }
        },
        {
          breakpoint: 1279,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 959,
          settings: {
            infinite: false,
            vertical: true,
            slidesToShow: serverdata !== null ? serverdata.collection[0].items.length : 4,
            dots: false,
          }
        }
      ]
    }

    const rectanglecateogory1 = {
      settings: "unslick",
      responsive: [
        {
          breakpoint: 9999,
          settings: "unslick",
        },
        {
          breakpoint: 959,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            lazyLoad: true,
            dots: true,
            infinite: false,
            arrows: false,
          }
        }
      ]
    }
    const bloglistlarge1 = {
      afterChange: (e) => this.carouselchange(e, 'bloglistindex'),
      responsive: [
        {
          breakpoint: 9999,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            lazyLoad: true,
            infinite: false,
            arrows: false,
            lazyLoad: true,
          }
        },
        {
          breakpoint: 959,
          settings: {
            slidesToShow: 1.1,
            slidesToScroll: 1,
            infinite: false,
            arrows: false
          }
        }
      ]
    }
    const newsArticle = {
      afterChange: (e) => this.carouselchange(e, 'bloghorindex'),
      responsive: [
        {
          breakpoint: 9999,
          settings: {
            slidesToShow: 1.6,
            lazyLoad: true,
            slidesToScroll: 1,
            infinite: false,
            arrows: false
          }
        },
        {
          breakpoint: 599,
          settings: {
            slidesToShow: 1.2,
            slidesToScroll: 1,
            infinite: false,
            arrows: false
          }
        },
      ]
    }
    return (
      <div className="home">
        <PageHeader />
        <main>

          {/*=============== banner start ==================*/}
          <div className="banner">
            <div className="bannerimage">
              <CustomImage image='https://images.travelxp.com/images/mauritius/mauritius/mauritiusbeachview.jpg' mobilewidth={414}
                mobileheight={280} tabletwidth={1127} tabletheight={624.95} desktopwidth={1215} desktopheight={624.95} largedesktopwidth={1728} largedesktopheight={863} />
            </div>
            <div className="absoluteitem">
              <h1>Explore the world</h1>
              <div className={this.state.showModal === "true" ? "overlayshow" : "overlayhide"} onClick={()=>this.hideModal()}></div>
              <div className="searchcontainer">
                <input type="text" placeholder='Search' className="searchbox" value={this.state.searchinput} onChange={(e)=>this.searchcity(e)}/>
                <img src={sitesettings.svgimageUrl + "/general/guidesearch.svg"} className="searchimage" alt="search" />
                <div className={this.state.showModal === "true" ? "autocompletemodal visible" : "novisible"}>
                {this.state.data.map((item,index)=>
                  <a href={item.href} key={index} className="searchdatalist">
                    <img src={item.img.url} alt={item.img.alt} />
                    <span>{item.categories}</span>&nbsp;
                  </a>
                )} 
                </div>
              </div>
              <a className={this.state.showModal === "true" && "moverightimage"}>
                <img src={sitesettings.svgimageUrl + "/general/spintheworld.svg"} alt="spin the world" className="spinimage" />
              </a>
            </div>
          </div>
          {/*=============== banner end ==================*/}

          <a className="scrolldown" onClick={() => this.scrollDown()} title="Scroll Down">
            <img src={sitesettings.svgimageUrl + "/general/arrowdownblack.svg"} alt="click to scroll" />
          </a>
          
          {serverdata.collection.map((item, index) =>
            <section key={index} id={item.type} ref={index === 0 && this.myDivSection}>

              {item.type === "carousel1" ?                
                <div className="carousel1 shortwrapper">
                  <div className="carouselleft">
                    <div>
                      <h2 className="carouselheader">{item.name}</h2>
                      <SliderButtons title="carousel" prevdisable={this.state.carouselindex <= 0} nextdisable={this.state.carouselindex === item.items.length - 2} onPrevious={this.previous} onNext={this.next} mobileshow="none" />
                    </div>
                    <GoButton title='View All' compclass='mobilenone' href='/guidearticles' />
                  </div>

                  <div className="trendingpicksslider">
                    <Slider ref={c => (this.carousel = c)} {...carousel1}>
                      {item.items.map((childitem, childindex) =>
                        <article key={childindex}>
                          <Carousel1
                            {...childitem} key={childindex + "c"} />
                        </article>
                      )}
                    </Slider>
                  </div>
                  <div className='gobutton desktopnone'>
                    <GoButton title='View All' href='/guidearticles' />
                  </div>
                </div>

                : item.type === "rectanglecateogory1" ?
                  <div className="shortwrapper hottopic">
                    <h2>{item.name}</h2>
                    <Slider {...rectanglecateogory1}>
                      {item.items.map((childitem, childindex) =>
                        <RectangleCategory1 {...childitem} key={childindex + "c"} />
                      )}
                    </Slider>
                    <div className="afterunslick">
                      {item.items.map((childitem, childindex) =>
                        childindex < 8 ?
                          <RectangleCategory1 {...childitem} key={childindex + "c"} />
                          : null
                      )}
                    </div>
                    <div className='gobutton mobilenone'>
                      <GoButton title='View All' href="/hottopic" />
                    </div>
                  </div>

                  : item.type === "bloghorizontal1" ?
                    <div className="bloghorizontalwrapper blogwrap">
                      <div className="carouselleft blogheaderwrap">
                        <div className="bloghorizontalheader">
                          <h2>{item.name}</h2>
                          <div className="blogauthorimage">
                            <img src={item.author.avatar} alt={item.author.caption} />
                            <span>{item.author.name}</span>
                          </div>
                          <div className="blogauthor">
                            <span>{item.date}</span>
                            <span>
                              <span className='dot'>&#8226;</span>
                              <span>{item.author.name}</span>
                            </span>
                          </div>
                        </div>
                        <SliderButtons title="bloghorizontal" prevdisable={this.state.bloghorindex <= 0} nextdisable={this.state.bloghorindex === item.items.length - 1} onPrevious={this.previous} onNext={this.next} mobileshow="none" />
                      </div>
                      <div className="blog">
                        <Slider ref={c => (this.bloghorizontal = c)} {...newsArticle}>
                          {item.items.map((childitem, childindex) =>
                            <BlogHorizontal {...childitem} key={childindex + "c"} index={childindex + 1} />
                          )}
                        </Slider>
                      </div>
                      <div className="slidebtnWrap">
                        <SliderButtons title="bloghorizontal" prevdisable={this.state.bloghorindex <= 0} nextdisable={this.state.bloghorindex === item.items.length - 1} onPrevious={this.previous} onNext={this.next} />
                      </div>
                    </div>
                    : item.type === "videosolo1" ?
                      <div className="wrapper">
                        <VideoSolo {...item.items[0]} />
                      </div>
                      : item.type === "blogsolo1" ?
                        <div className="shortwrapper">
                          {item.items.map((childitem, childindex) =>
                            <BlogSolo1 {...childitem} key={childindex} index={childindex + 1} />
                          )}
                          <div className='gobutton desktopnone'>
                            <GoButton title='View More' />
                          </div>
                        </div>

                        : item.type === "bloglistlarge1" &&
                        <div className="shortwrapper">
                          <div className="blogheader">
                            <h2>{item.title}</h2>
                          </div>
                          <div className="spacebtwslide">
                            <Slider ref={c => (this.bloglistlarge = c)} {...bloglistlarge1}>
                              {item.items.map((childitem, childindex) =>
                                <BlogListLarge1 {...childitem} key={childindex + "c"} />
                              )}
                            </Slider>
                          </div>
                          {item.items.length > 2 &&
                            <div className="centerbtnwrap">
                              <SliderButtons title="bloglistlarge" prevdisable={this.state.bloglistindex <= 0} prevnext={this.state.bloglistindex === item.items.length - 2} onPrevious={this.previous} onNext={this.next} mobileshow="none" />
                            </div>
                          }
                          <div className='gobutton'>
                            <GoButton title='View All' />
                          </div>
                        </div>
              }
            </section>
          )}
        </main>
        <style jsx>{`
          .banner { position: relative; }
          .bannerimage { height: 38vh; }
          .mobilenone { display: none; }
          
          #carousel1 { 
            margin-top: 25px; 
            padding-bottom: 45px;
          }
          .shortwrapper{ 
            width: 94%;
            margin: 0 auto;
          }
          .bloghorizontalwrapper{ padding-left:3%;}
          .searchcontainer,
          .scrolldown,
          .autocompletemodal
          { display: none; }

          .afterunslick { display: none; }
          .gobutton {text-align: center; }
          .carouselheader { 
            text-align: center; 
            line-height: 40px;
          }

          .slidebtnWrap { 
            text-align: center; 
            margin-top: 20px;
          }
          .centerbtnwrap{
            display:none;
          }
          .absoluteitem {
            position: absolute;
            top: 20%;
            width: 100%;
            height: 80%;
            text-align: center;
          }
          h1 {
            color: ${theme.colors.white};
            font-size: ${theme.fontsize.largestFont};
            font-weight: ${theme.fontWeight.extrabold};
            margin-bottom: 23px;
          }
          h2 {
            font-size: ${theme.fontsize.largestFont};
            font-weight: ${theme.fontWeight.semiextrabold};
            margin-bottom: 25px;
          }
          .bloghorizontalheader h2{
            margin-bottom:10px;
          }
          .bloghorizontalheader{
            margin-bottom:25px;
          }
          .blogauthor{
            color: ${theme.colors.darkgray};
            font-size: ${theme.fontsize.exsmallFont};
          }
          .dot {margin-right: 10px;}
          .blogauthorimage{ display:none; }
          .blogauthor>span{
            margin-right:4%;
          }
          #rectanglecateogory1 {
            background-color: ${theme.colors.pinkwhite};
            padding: 15px 0 30px;
            margin-bottom: 38px;
          }
          #bloghorizontal1,#bloglistlarge1{
            margin-bottom: 38px;
          }
          #blogsolo1 {
            margin-bottom: 125px;
          }
          .blogheader { text-align: center; }
          .trendingpicksslider { padding-bottom: 15px; }
          .spacebtwslide { margin-bottom: 30px; }
          #videosolo1 {margin-bottom: 50px;}
          article {
            margin-bottom: 30px;
          }

          @media screen and (min-width: 680px) {
            .shortwrapper{ 
              width:600px; 
              margin: 0 auto; 
            }
            
          }
          @media screen and (min-width: 960px) {
            .bloghorizontalwrapper{ padding-left: 0;}
            .shortwrapper { width: 856px; }
            .mobilenone { display: block; } 
            .spacebtwslide{
              margin-left: -10px;
              margin-right: -10px;
            }
            .desktopnone { display: none; }

            article {
              margin-left: 10px;
              margin-bottom: 0;
              width: 95% !important;
            }
            
            main { padding-top: 7px; }
            h2 { font-size: ${theme.fontsize.extraLargeFont}; }
            .hottopic h2 { text-align: center; }
            .trendingpicksslider { width: calc(100% - 300px); }
            .carouselheader { text-align: left; }
            .blogheader { text-align: center; margin-bottom:25px;}
            .slidebtnWrap { display: none; }
            .blogwrap { display: flex; }
            
            .carouselleft {
              display: flex; 
              flex-direction: column;
              align-items: flex-start;
              justify-content: space-between;
              width: 300px;
              padding: 20px 0;
            }
            .blogheaderwrap { 
              width: 320px; 
              margin-right: 20px; 
            }
            .blogauthorimage{
              display:flex;
              align-items: center;
              margin-bottom: 15px;
            }
            .blogauthorimage span{
              margin-left: 8px; 
              color: ${theme.colors.black};
              font-size: ${theme.fontsize.smallestFont};
            }
            .blogauthorimage img{
              width:22px;
              height:22px;
              border-radius:50%;
            }
            .bloghorsubheader{
              margin:10px 0;
            }
            .bannerimage { height: 82vh; }
            .spinimage { width: 50px; }

            .blog {
              width: calc(100% - 340px);
            }
            #carousel1 { 
              margin-top: 60px; 
              margin-bottom: 80px;
            }
            #rectanglecateogory1 { background-color: transparent; }
            .carousel1 { display: flex; }
            .scrolldown {
              width: 20px;
              margin: 0 auto;
            }
            .scrolldown img { width: 100%; }
            
            .searchcontainer { 
              display: block;
              position: relative; 
              width: 568px;
              margin: 0 auto;
              z-index:2;
            }
            .autocompletemodal{
                display:block;
                position:absolute;
                width:90%;
                top:61px;
                left:5%;
                box-shadow: rgba(0, 0, 0, 0.063) 0px 10px 20px 0px;
                border-radius: 0 0 12px 12px;
                background-color:#f9f5f2;
                overflow:auto;
                padding:20px;
                max-height:50vh;
            }
            .novisible{ 
              visibility: hidden;
              height:0;
            }
            .visible{ visibility:visible;}
            .overlayhide{ display:none; }
            .overlayshow{
              display: block;
              position: fixed;
              width: 100%;
              top: 0;
              left: 0;
              right: 0;
              bottom: 0;
              z-index: 1;
            }
            .moverightimage{
              position:absolute;
              top: 85%;
              left: 91%;
            }
            .searchimage {
              position: absolute;
              top: 20px;
              right: 26px;
            }
            .searchdatalist{
              display:flex;
              justify-content:start;
              align-items:center;
              padding:5px 0;
              color:black;
            }
            .searchdatalist img{
              width:30px;
              height:30px;
              border-radius:100%;
              margin-right:20px;
            }
            
            h1 { 
              font-size: ${theme.fontsize.bigFont};
              margin-bottom: 26px;
             }
            .searchbox {
              border: none;
              width: 100%;
              display: block;
              font-size: ${theme.fontsize.mediumFont};
              font-family: 'Lato',sans-serif;
              letter-spacing: 1.2px;
              padding: 20px 27px;
              border-radius: 30px;
              margin-bottom: 30px; 
              outline: none;
            }

            .scrolldown {
              display: block;
              text-align: center;
              margin-top: 5px;
            }
            
            .banner {
              width: 91%;
              margin: 0 auto;
            } 
            
            .afterunslick { 
              display: flex; 
              flex-wrap: wrap;
              margin-bottom: 13px;
            }
            .centerbtnwrap{
              display:block;
              text-align: center; 
              margin-bottom: 25px;
            }
            
          }
          @media screen and (min-width: 1280px) {
            .blogwrap { 
              width: 1039px; 
              margin-left: auto;
            }
            .shortwrapper { width: 900px; }
          }
          @media screen and (min-width: 1366px) {
            .bloghorizontalwrapper{
              margin-left: auto;
              width: 1167px;
            }
          }
          @media screen and (min-width: 1680px) {
            .shortwrapper { width: 1100px; }
            .spinimage { width: 60px; }
            .searchcontainer { width: 710px; }
            .searchbox { border-radius: 40px; }
            main { font-size: ${theme.fontsize.largeFont}; }
            article { margin-left: 36px; }
          }
          @media screen and (min-width: 1920px) {
            .shortwrapper { width: 1300px; }
            article { margin-left: 45px; }
          }
      `}</style>
        <style global jsx>{`
          html {
            scroll-behavior: smooth;
          }
        `}</style>
      </div>
    )
  }
}
