import React from 'react';
import sitesettings from '../config/sitesettings';
import PageHeader from '../components/common/pageheader';
import CustomImage from '../components/common/customimage';
import theme from '../config/theme';
import RectangleCategory1 from '../components/rectanglecategory1';

export default class HotTopic extends React.PureComponent{
    render(){
        const hotTopicData = sitesettings.hotTopicPage;
        console.log(hotTopicData);
        return(
            <React.Fragment>
                <div className="banner">
                    <PageHeader headerClass='nobg absolute' showsearchbox="true" />
                    <h1>{hotTopicData.banner.title}</h1>
                    <div className="backgroundImage">
                        <CustomImage image={hotTopicData.banner.img.url} />
                    </div>
                </div>
                <div className="wrapper">
                    {hotTopicData.items.map((item,index)=>
                        <div className="hotTopicContainer" key={index}>
                            <RectangleCategory1  {...item} hottopiceffect="true"/>
                        </div>
                    )}
                </div>
                <style jsx>{`
                    .banner{
                        position:relative;
                    }
                    .backgroundImage{
                        height:160px;
                    }   
                    h1 
                    {
                        width: 100%;
                        text-align: center;
                        color: ${theme.colors.white};
                        font-size : ${theme.fontsize.largestFont};
                        font-weight: ${theme.fontWeight.bold};
                        position: absolute;
                        bottom: 34px;
                    }
                    .hotTopicContainer{
                        margin-bottom:12px;
                    }
                    .wrapper{
                        margin-top:12px;
                    }

                    @media screen and (min-width:960px){
                        .backgroundImage{
                            height:220px;
                        }
                        h1{
                            font-size:${theme.fontsize.extraExtraLargeFont};
                            bottom:50px;
                        }
                        .wrapper{
                            width:800px;
                            display:flex;
                            justify-content:space-between;
                            flex-wrap:wrap;
                            margin:100px auto;
                        }
                        .hotTopicContainer{
                            width:48%;
                            height:300px;
                            margin-bottom:4%;
                        }
                    }

                    @media screen and (min-width:1280px){
                        .wrapper{ width:900px }
                    }

                    @media screen and (min-width:1366px){
                        .wrapper{ width:1100px }
                    }
                    @media screen and (min-width:1680px){
                        .wrapper{ width:1280px }
                    }
                    @media screen and (min-width:1920px){
                        .wrapper{ width:1400px }
                    }
                     
                `}</style>
                
            </React.Fragment>
        )
    }
}