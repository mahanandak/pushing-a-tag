import React from 'react';
import theme from '../config/theme';
import CustomImage from './common/customimage';


export default React.memo(function GuestCard(props){
  return (
      <article className="container">
          <div className="imgholder">
            <CustomImage image={props.img.url} alt={props.img.url} href={props.img.href}  mobilewidth={270}
                        mobileheight={100} tabletwidth={295} tabletheight={500} desktopwidth={320} desktopheight={500} largedesktopwidth={380} largedesktopheight={800}/>
            <img src="https://images.travelxp.com/images/txpin/vector/general/circle.svg" alt="circle"/>
          </div>
          <h3 className="description">{props.title}</h3>

          <style jsx>{`
              .container{
                width:48%;
                padding:10px;
                background-color: ${theme.colors.white};
                box-shadow: rgba(0, 0, 0, 0.063) 0px 10px 20px 0px;
                margin:0 4% 10px 0;
                display:inline-block;
              }
              .container:nth-child(even){
                margin-right:0;
              }
              .imgholder{
                  width:100%;
                  height:100px;
                  position:relative;
              }
              .imgholder img{
                  position:absolute;
                  top:10px;
                  right:10px;
              }
              .description{
                padding-top:5%;
              }  
              h3{
                  font-size:${theme.fontsize.smallerFont};
                  font-weight: ${theme.fontWeight.bold};
                  color: ${theme.colors.black};
                  text-align:center;
              }
              
              @media screen and (min-width:960px){
                .container{
                    width:19%;
                    height:auto;
                    border-radius:12px;
                    margin:0px 1% 20px 0px;
                }
                .container:nth-child(even){margin:0px 1.3% 20px 0px}
                .container:nth-child(5n){margin:0 0 20px 0}
                .imgholder{
                  border-radius:12px;
                  overflow:hidden;
                  height:200px
                }
                h3{font-size:${theme.fontsize.normalFont}}
              }
          `}</style>
      </article>
  )
})
