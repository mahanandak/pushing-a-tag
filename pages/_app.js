import Layout from "../components/layout"
import App from 'next/app'
import { DefaultSeo, SocialProfileJsonLd } from 'next-seo';

class MyApp extends App {
    render() {
        const { Component, pageProps } = this.props
        return (
            <React.Fragment>
                {/* <DefaultSeo
                    dangerouslySetAllPagesToNoIndex={true}
                /> */}
                < SocialProfileJsonLd
                    type="Organization"
                    name="travelxp"
                    url="http://dev.travelxp.guide"
                    sameAs={
                        [
                            'https://www.facebook.com/travelxpcom/',
                            'https://www.instagram.com/travelxpdotcom/',
                            'https://twitter.com/Travelxpdotcom'
                        ]}
                />
                <Layout>
                    <Component {...pageProps} />
                </Layout>
            </React.Fragment>
        )
    }
}

export default MyApp