
import sitesettings from "../../config/sitesettings"
import theme from "../../config/theme"

export default React.memo(function PageFooter(props) {
    return (
        <footer>
            <div className="icon">
                <a target='_blank' href='https://www.facebook.com'>
                    <img src="https://images.travelxp.com/images/txpin/vector/general/facebookcircle.svg" />
                </a>
                <a target='_blank' href='https://www.twitter.com'>
                    <img src="https://images.travelxp.com/images/txpin/vector/general/twittercircle.svg" />
                </a>
                <a target='_blank' href='https://www.instagram.com'>
                    <img src="https://images.travelxp.com/images/txpin/vector/general/instagramcircle.svg" />
                </a>
                <a target='_blank' href='https://www.youtube.com'>
                    <img src="https://images.travelxp.com/images/txpin/vector/general/youtubecircle.svg" />
                </a>
                <a target='_blank' href='https://www.pinterest.com'>
                    <img src="https://images.travelxp.com/images/txpin/vector/general/pinterestcircle.svg" />
                </a>
            </div>
            <div className="copyright">©Copyright  <a href='/'>Travelxp.</a> All Rights Reserved</div>
            <style jsx>{`
                footer
                {  
                    display:flex;
                    flex-direction: column;
                    // background-image: url("../../static/images/blog-footer.jpg");
                    background-image: url("http://thynkcloud.com/img/banner-bg.jpg");
                    background-position: center;
                    background-size:cover;
                    background-repeat: no-repeat;
                    padding-top: 203px;
                }            
              
                .icon
                {
                    display: flex;
                    justify-content: center;           
                    border-bottom:1px solid #ccc;
                    padding-bottom: 18px;
                    width: 96%;
                    margin: 0 auto;
                }
                .icon a { margin-right: 25px; }
                .icon a:last-child { margin-right: 0; }
                
                .copyright
                {
                    display:flex; 
                    justify-content: center;
                    color: ${theme.colors.darkgray}; 
                    font-size: ${theme.fontsize.smallestFont};
                    padding: 15px 0;
                }
                .copyright a{
                    margin: 0 5px;
                    color: ${theme.colors.highlightcolor};
                }
                @media screen and (min-width: 960px) {
                    a{
                        transition: all 0.3s ease-in-out;
                    }
                    a:hover {
                        transform: scale(1.1);
                    }
                    footer {
                        padding-top: 230px;
                    }
                    .copyright { padding-bottom: 30px; }
                    .copyright a:hover {
                        text-decoration: underline;
                    }
                }
            `}</style>
        </footer>
    )
})