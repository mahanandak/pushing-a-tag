import React from 'react';
import theme from '../config/theme';
import CustomImageWithoutLazyLoad from './common/customimagewithoutlazylaod';
import Link from "next/link"

export default React.memo(function GuestCard(props) {
  const {hottopiceffect} =props;
  return (
    <article className={hottopiceffect === "true" ? "hotTopicHolder" : "maincontainer"}>
      <Link href={props.img.href}>
        <a className={hottopiceffect === "true" ? "hotTopicontainer" : "container"}>
          <CustomImageWithoutLazyLoad image={props.img.url} alt={props.img.alt} mobilewidth={164}
            mobileheight={90} tabletwidth={198} tabletheight={131} desktopwidth={266} desktopheight={131} largedesktopwidth={383} largedesktopheight={200} />
          <div className="overlay"></div>
        </a>
      </Link>
      <h3 className='description'>{props.title}</h3>
      <style jsx>{`
              .maincontainer { 
                margin-right: 12px; 
                position: relative;
              }
              .hotTopicHolder{
                position:relative;
                margin-bottom:12px;
              }
              .hotTopicontainer{
                position:relative;
                width:100%;
                height:180px;
              }
              .container{
                height:110px;
                position:relative;
              }
              .overlay{
                height: 100%;
                width: 100%;
                position: absolute;
                top:0;
                background: ${theme.colors.overlay};
              }
              .description{
                position:absolute;
                top:50%;
                left:50%;
                transform: translate(-50%, -50%);
              }   
              h3{
                  font-size:${theme.fontsize.smallerFont};
                  font-weight: ${theme.fontWeight.extrabold};
                  font-family: 'Montserrat',sans-serif;
                  color: ${theme.colors.white};
              }
              a { color: ${theme.colors.black}; }
              
              @media screen and (min-width:960px){
                .hotTopicHolder{
                  margin:0;
                  height:100%;
                }
                .maincontainer{ 
                  width: 23.5%; 
                  height: 131.78px;
                  margin-right: 2%;
                  margin-bottom: 2%;
                }
                .maincontainer:nth-child(4n) {
                  margin-right: 0;
                }
                .container{
                    width: 100%;
                    height: 100%;
                }
                .hotTopicontainer{
                  height:100%;
                }
                h3{
                    font-weight: ${theme.fontWeight.semiextrabold};
                }  
                .hotTopicHolder h3{
                  font-size:${theme.fontsize.largerFont};
                }
              }
              @media screen and (min-width:1680px){
                .maincontainer { height: 185px; }
              }
              @media screen and (min-width:1680px){
                .maincontainer { height: 200px; }
              }
          `}</style>
    </article>
  )
})
