import React from 'react'
import PropTypes from 'prop-types';

export default class CustomImage extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            currentImage: '../../static/images/thumbnail.png',
        }
    }


    async componentDidMount() {
        this.setImage()
    }

    setImage() {
        var isTablet = window.matchMedia('(min-width: 960px)');
        var isDesktop = window.matchMedia('(min-width: 1366px)');
        var isLargeDesktop = window.matchMedia('(min-width: 1680px)')

        const { image,mobileheight, mobilewidth, tabletheight, tabletwidth, desktopheight, desktopwidth, largedesktopwidth, largedesktopheight } = this.props;
        var imgwidth, imgheight;
        if (isLargeDesktop.matches) {
            imgwidth = largedesktopwidth;
            imgheight = largedesktopheight;
        }
        else if (isDesktop.matches) {
            imgwidth = desktopwidth;
            imgheight = desktopheight;
        }
        else if (isTablet.matches) {
            imgwidth = tabletwidth;
            imgheight = tabletheight;
        }
        else {
            imgwidth = mobilewidth;
            imgheight = mobileheight;
        }

        let imagesrc;
        if (typeof (image) === "string") {
            imagesrc = image.indexOf('images.travelxp.com') !== -1 ? image + '?tr=w-' + imgwidth + ',h-' + imgheight : image;
        }
        else if (typeof (image) === "object") {
            imagesrc = ""
        }
        else {
            imagesrc = image
        }
        this.setState({
            currentImage: imagesrc
        })
    }

    render() {
        return (
            <React.Fragment>
                <img src={this.state.currentImage} className="lzy_img" />
                <style jsx>{`
          img {
            width: 100%;
            height: 100%;
            object-fit: cover;
          }
        `}</style>
            </React.Fragment>
        )
    }
}


CustomImage.propTypes = {
    image: PropTypes.string.isRequired,
    mobilewidth: PropTypes.number.isRequired,
    mobileheight: PropTypes.number.isRequired,
    tabletwidth: PropTypes.number.isRequired,
    tabletheight: PropTypes.number.isRequired,
    desktopwidth: PropTypes.number.isRequired,
    desktopheight: PropTypes.number.isRequired,
    largedesktopwidth: PropTypes.number.isRequired,
    largedesktopheight: PropTypes.number.isRequired
};