import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class extends Document {
  render() {
    return (
      <Html>
        <Head>
          <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800,900&display=swap" rel="stylesheet" />
          <link href="https://fonts.googleapis.com/css?family=Lato:300,400,600,700,800,900&display=swap" rel="stylesheet"></link>
          <link rel="shortcut icon" type="image/x-icon" href="//images.travelxp.com/images/txpin/web/favicon_16x16.ico" />
          <link rel="stylesheet" type="text/css" charSet="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" /> 
          <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />

          {/* <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=IntersectionObserver"></script> */}
          {/* <script crossorigin="anonymous" src="https://polyfill.io/v3/polyfill.min.js?features=IntersectionObserver"></script> */}
          {/* <script src="https://polyfill.io/v3/polyfill.min.js?features=IntersectionObserver"></script> */}
          {/* <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> */}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>

      </Html>
    )
  }
}