import sitesettings from '../../config/sitesettings'
import theme from '../../config/theme'
import Link from 'next/link'

export default React.memo(function GoButton(props) {
    const { compclass, title, href } = props
    return (
        <Link href={href !== undefined ? href : '/'}>
            <a className={compclass !== undefined ? compclass : null}>
                {title}
                <img src={sitesettings.svgimageUrl + "/general/long-arrow-next.svg"} />
                <style jsx>{`
                    a {
                        display: inline-flex;
                        align-items: center;
                        justify-content: center;
                        border: 1px solid ${theme.colors.lightgray};
                        border-radius: 25px;
                        font-weight: ${theme.fontWeight.bold};
                        height: 50px;
                        width: 200px;
                        color: ${theme.colors.black};
                    }
                    img {
                        width: 20px;
                        margin-left: 20px;
                    }
                    .mobilenone { display: none;}
                    @media screen and (min-width: 960px) {
                        .mobilenone { display: inline-block;}
                        a {
                            height: auto;
                            width: auto;
                            font-size: ${theme.fontsize.smallestFont};
                            padding: 9px 26px;
                        }
                        img {
                            width: 15px;
                            margin-left: 15px;
                        }
                        a:hover {
                            opacity: 0.7;
                        }
                    }
                `}</style>
            </a>
        </Link>
    )
})
