
import sitesettings from "../../config/sitesettings"
import theme from "../../config/theme"
import Link from "next/link"

export default React.memo(function PageHeader(props) {
    const {headerClass, showsearchbox, searchwithborder} = props;
    return (
        <header className={headerClass !== undefined && headerClass}>
            <Link href="/">
                <a className='logo'>
                    <img src={sitesettings.svgimageUrl + "/general/travelxplogo.svg"} alt="Travelxp" className="logoimg" />
                </a>
            </Link>
            { showsearchbox === "true" &&
                <div className="searchcontainer">
                    <input type="text" placeholder='Search' className={ searchwithborder === "true" ? "searchboxwithborder": "searchbox"} />
                    <a>
                        <img src={sitesettings.svgimageUrl + "/general/guidesearch.svg"} className="searchimage" alt="search" />
                    </a>
                </div>
            }
            <div className="rightimages">
                <a className='search'>
                    <img src={sitesettings.svgimageUrl + "/general/guidesearch.svg"} className="searchicon" />
                </a>
                <a>
                    <img src={sitesettings.svgimageUrl + "/general/circleduser.svg"} className="usericon" />
                </a>
            </div>
            <style jsx>{`
                header {
                    position: sticky;
                    top: 0;
                    left: 0;
                    right: 0;
                    background-color: ${theme.colors.white};
                    padding: 15px 0 0;
                    z-index: 1;
                    display: flex;
                    justify-content: space-between;
                    align-items:center;
                }
                .logo { margin-left: 24px; }
                .logoimg {
                    height: 25px;
                }
                .rightimages {
                    display: flex;
                    align-items: center;
                    margin-right: 10px;
                }
                .search { margin-right: 12px; }
                .nobg { background-color: transparent; }
                .absolute {
                    position: absolute;
                    width: 100%;
                }
                .searchcontainer{ display:none; }
                @media screen and (min-width: 960px) {
                    .wrapper { width: 97%; }
                    .logoimg { height: 28px;}
                    .searchicon { display: none; }
                    .usericon { width: 38px; }
                    .searchcontainer{
                        display: block;
                        position: relative; 
                        width: 60%;
                    }
                    .searchbox {
                        border: none;
                        width: 100%;
                        display: block;
                        font-size: ${theme.fontsize.mediumFont};
                        font-family: 'Lato',sans-serif;
                        letter-spacing: 1.2px;
                        padding: 20px 27px;
                        border-radius: 30px;
                        outline: none;
                    }
                    .searchboxwithborder {
                        border: 1px solid ${theme.colors.lightgray};
                        box-shadow: rgba(0,0,0,0.063) 0px 10px 20px 0px;
                        width: 100%;
                        display: block;
                        font-size: ${theme.fontsize.mediumFont};
                        font-family: 'Lato',sans-serif;
                        letter-spacing: 1.2px;
                        padding: 20px 27px;
                        border-radius: 30px;
                        outline: none;
                    }
                    .searchimage {
                        position: absolute;
                        top: 20px;
                        right: 26px;
                    }
                }
                }
            `}</style>
        </header>
    )
})