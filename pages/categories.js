import React from 'react';
import sitesettings from '../config/sitesettings';
import dynamic from 'next/dynamic';
import PageHeader from '../components/common/pageheader';
import theme from '../config/theme';
import Slider from 'react-slick';

const CustomImage = dynamic(import("../components/common/customimage"));
const HolidayTopicCard = dynamic(import("../components/holidaytopiccard"));
const SliderPrevButton = dynamic(import("../components/common/sliderprevbutton"));
const SliderNextButton = dynamic(import("../components/common/slidernextbutton"));
const PlaceCard = dynamic(import("../components/placecard"));
const Carousel1 = dynamic(import("../components/carousel1"));
const BestExperienceCard = dynamic(import("../components/bestexperiencecard"));

export default class Categories extends React.PureComponent{
    constructor(props){
        super(props);
        this.state = {
            holidaytopicindex:0,
            carouselindex:0,
            experienceindex:0,
            x:6.5
        }
    }
    previous=(type)=>{
        if(type === "holidaytopiccard"){
            this.holidaytopiccard.slickPrev();
        }
        else if(type === "carousel1"){
            this.carousel1.slickPrev();
        }
        else if(type === "bestexperiencecard"){
            this.bestexperiencecard.slickPrev();
            this.setState({x:this.state.x+6.6})
        }
    } 
    next = (type) =>{
        if(type === "holidaytopiccard"){
            this.holidaytopiccard.slickNext();
        }
        else if(type === "carousel1"){
            this.carousel1.slickNext();
        }
        else if(type === "bestexperiencecard"){
            this.bestexperiencecard.slickNext();
            this.setState({x:this.state.x-6.6})
        }
    }
    carouselchange = (e, name) => {
        console.log(e)
        this.setState({ [name]: e })
    }
    initial = (e) =>{
        console.log(e)
    }
    render(){
        const bestexperiencecard = {
            afterChange: (e) => this.carouselchange(e, 'experienceindex'),
            beforeChange: (e) => this.initial(e),
            responsive:[
                {
                    breakpoint: 9999,
                    settings: {
                    //   centerMode: true,
                    //   centerPadding: '60px',
                      infinite: false,
                    //   slidesToShow:3,
                      slidesToScroll: 1,
                      arrows: false,
                      speed:700
                    }
                },
                {
                    breakpoint:959,
                    settings:{
                        infinite: false,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false   
                    }
                }
            ]
        }
        const holidaytopiccard = {
            afterChange: (e) => this.carouselchange(e, 'holidaytopicindex'),
            responsive: [
                {
                    breakpoint: 9999,
                    settings: {
                      infinite: false,
                      slidesToShow: 3,
                      slidesToScroll: 1,
                      arrows: false,
                    }
                },
                {
                  breakpoint: 959,
                  settings: {
                    infinite: false,
                    slidesToShow: 1.1,
                    slidesToScroll: 1,
                    arrows: false,
                  }
                }
            ]
        }

        const carousel1 = {
            afterChange: (e) => this.carouselchange(e, 'carouselindex'),
            responsive: [
                {
                    breakpoint: 9999,
                    settings: {
                      infinite: false,
                      slidesToShow: 2,
                      slidesToScroll: 1,
                      arrows: false
                    }
                },
                {
                  breakpoint: 959,
                  settings: {
                    infinite: false,
                    slidesToShow: 1.1,
                    slidesToScroll: 1,
                    arrows: false,
                  }
                }
            ]
        }
        return(
            <div>
                <main>
                    {/* -----------------------------Banner start------------------------------------------ */}
                    <div className="banner">
                        <div className="bannerimage">
                            <CustomImage image={sitesettings.articlecategory.banner.img} mobilewidth={398}
                                mobileheight={398} tabletwidth={768} tabletheight={624.95} desktopwidth={1215} desktopheight={624.95} largedesktopwidth={1728} largedesktopheight={863} />
                        </div>
                        <div className="absoluteitem">
                            <PageHeader headerClass='nobg absolute' showsearchbox="true"/>
                            <div className="bannerwrap">
                                <span className="bannerspacebelow">{sitesettings.articlecategory.banner.subtitle}</span>       
                                <h1 className="bannerspacebelow">{sitesettings.articlecategory.banner.name}</h1>
                                <p className="bannerspacebelow">{sitesettings.articlecategory.banner.description}</p>
                            </div>
                        </div>
                    </div>
                    {/* -----------------------------Banner End------------------------------------------ */}

                    {sitesettings.articlecategory.collection.map((item,index)=>
                        <section key={index} id={item.type}>
                            { item.type === "bestexperiencecard" ?
                            <div>
                                <div className="bigwrapper">
                                    <span className="cardsubheading">{item.subtitle}</span>
                                    <h2 className="cardheading">{item.name}</h2>
                                    <div className="btnaroundslider">
                                        <div className="absPrevBtn">
                                            <SliderPrevButton title="bestexperiencecard" prevdisable={this.state.experienceindex <= 0} onPrevious={this.previous} onNext={this.next} />
                                        </div>
                                        <div className="experinceslidder">
                                            <Slider ref={c => (this.bestexperiencecard = c)} {...bestexperiencecard} >
                                                {item.items.map((childitem,childindex)=>
                                                    <BestExperienceCard {...childitem} key={childindex + "c"}/>
                                                )}
                                            </Slider>   
                                        </div>
                                        <div className="absNextBtn">
                                            <SliderNextButton title="bestexperiencecard" nextdisable={this.state.experienceindex === item.items.length-1 } onPrevious={this.previous} onNext={this.next} />    
                                        </div>
                                    </div>
                                </div>
                                <div className="descriptioncontainer">
                                    <div className="upArrow"></div>
                                    <div className="descriptionwrapper">
                                        <video poster={item.items[this.state.experienceindex].information.video.thumbnail} controls>
                                            <source src={item.items[this.state.experienceindex].information.video.sources[0]} type="video/mp4"></source>
                                        </video>
                                        <div>
                                            <h2>{item.items[this.state.experienceindex].title}</h2>
                                            <p>{item.items[this.state.experienceindex].information.description}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            :item.type === "holidaytopiccard" ?
                                <div className="backgroundset">
                                    <div className="slidderwrap">
                                        <span className="cardsubheading">{item.subtitle}</span>
                                        <h2 className="cardheading">{item.name}</h2>
                                        <div className="sliderbtwbutton">
                                            <SliderPrevButton title="holidaytopiccard" prevdisable={this.state.holidaytopicindex <= 0} nextdisable={this.state.holidaytopicindex === item.items.length-3 } onPrevious={this.previous} onNext={this.next} mobileshow="none"/>
                                        <div className="slidderholder">
                                        <Slider ref={c => (this.holidaytopiccard = c)} {...holidaytopiccard}>
                                            {item.items.map((childitem,childindex)=>
                                                <HolidayTopicCard {...childitem} key={childindex + "c"} />
                                            )}
                                        </Slider>
                                        </div>
                                            <SliderNextButton title="holidaytopiccard" prevdisable={this.state.holidaytopicindex <= 0} nextdisable={this.state.holidaytopicindex === item.items.length-3} onPrevious={this.previous} onNext={this.next} mobileshow="none"/>
                                        </div>
                                    </div>
                                </div>

                            : item.type === "placecard" ?
                                <div className="shortwrapper">
                                    <span className="cardsubheading">{item.subtitle}</span>
                                    <h2 className="cardheading">{item.name}</h2>
                                    <div>
                                        {item.items.map((childitem,childindex)=>
                                            <PlaceCard {...childitem} key={childindex+"c"} index={childindex+1} />
                                        )}
                                    </div>
                                </div>
                            
                            : item.type === "carousel1" &&
                                <div className="articlemaincontainer">
                                    <div className="toparticlewrap">
                                        <span className="cardsubheading">{item.subtitle}</span>
                                        <h2 className="cardheading">{item.name}</h2>
                                        <div className="sliderbtwbutton">
                                            <SliderPrevButton title="carousel1" prevdisable={this.state.carouselindex <= 0} nextdisable={this.state.carouselindex === item.items.length-2 } onPrevious={this.previous} onNext={this.next} mobileshow="none"/>
                                        <div className="slidderholder">
                                        <Slider ref={c => (this.carousel1 = c)} {...carousel1}>
                                            {item.items.map((childitem,childindex)=>
                                                    <Carousel1 {...childitem} key={childindex + "c"} showmobilesilder="true"/>
                                            )}
                                        </Slider>
                                        </div>
                                            <SliderNextButton title="carousel1" prevdisable={this.state.carouselindex <= 0} nextdisable={this.state.carouselindex === item.items.length-2} onPrevious={this.previous} onNext={this.next} mobileshow="none"/>
                                        </div>
                                    </div>
                                </div>
                            }
                        </section>
                    )}
                </main>
                <style jsx>{`
                    .banner{ position:relative; }
                    .bannerimage{
                        width:100%;
                        height:38vh;
                    }
                    .absoluteitem{
                        position:absolute;
                        top:0;
                        color:white;
                        text-align:center;
                        width:100%;
                    }
                    .bannerwrap{
                        width:60%;
                        margin:0 auto;
                        padding-top:10vh;
                    }
                    .bannerwrap span{
                        display:inline-block;
                        font-size:${theme.fontsize.smallerFont};
                        color:${theme.colors.halfwhite};
                        font-weight:${theme.fontWeight.light};
                        font-family:'Montserrat',sans-serif;
                    }
                    .bannerwrap h1{
                        font-size:${theme.fontsize.largestFont};
                        font-weight:${theme.fontWeight.extrabold};
                        font-family:'Montserrat',sans-serif;
                    }
                    .bannerwrap p{
                        font-size:${theme.fontsize.smallerFont};
                        color:${theme.colors.halfwhite};
                        font-family:'Latto',sans-serif;
                    }
                    .bannerspacebelow{ margin-bottom:5px; }
                    .backgroundset{
                        background-image:url("./static/images/web-blog-details.svg");
                    }
                    .slidderwrap,
                    .toparticlewrap{ 
                        width:96%;
                        margin-left:4%;
                    }
                    .slidderwrap{ padding:50px 0 100px; }
                    .toparticlewrap{ padding: 50px 0;}
                    .articlemaincontainer{ 
                        background-color:${theme.colors.black};
                        color:white;
                    }
                    .cardheading{
                        font-family:'Montserrat',sans-serif;
                        font-size:${theme.fontsize.largestFont};
                        font-weight:${theme.fontWeight.extrabold};
                        margin-bottom:30px;
                        text-align:center;
                    }
                    .cardsubheading{
                        display:inline-block;
                        color:${theme.colors.darkgray};
                        font-size:${theme.fontsize.mediumFont};
                        font-weight:${theme.fontWeight.light};
                        font-family:'Montserrat',sans-serif;
                        margin-bottom:10px;
                        width:100%;
                        text-align:center;
                    }
                    .shortwrapper,
                    .bigwrapper{
                        width:94%;
                        margin: 0 auto;
                    }
                    .shortwrapper{ padding:50px 0;}
                    .bigwrapper{padding:30px 0;}
                    .btnaroundslider{
                        display:flex;
                        justify-content:space-between;
                        align-items:center;
                    }
                    
                    .experinceslidder{
                        padding:30px 0 0;
                        width:75%;
                    }
                    .descriptioncontainer{
                        background-color:#f9f5f2;
                        position:relative;
                    }
                    video{
                        width:100%;
                    }
                    .descriptionwrapper{
                        width:94%;
                        margin:0 auto;
                        padding:30px 0;
                    }
                    .descriptionwrapper h2{
                        margin:20px 0 10px;
                    }
                    .descriptionwrapper p{color:${theme.colors.darkgray};}
                    .upArrow{
                        width: 0; 
                        height: 0; 
                        border-left: 15px solid transparent;
                        border-right: 15px solid transparent;
                        border-bottom: 20px solid #f9f5f2;
                        position:absolute;
                        top:-20px;
                        left:47%;
                    }

                    @media screen and (min-width:680px){
                        .bannerimage{ height:65vh; }
                        .absoluteitem{ padding-top:17vh }
                    }

                    @media screen and (min-width:960px){
                        .bannerimage{ height:82vh; }
                        .bannerwrap h1{ font-size:${theme.fontsize.bigFont};}
                        .bannerwrap p{ font-size:${theme.fontsize.extraLargeFont};}
                        .bannerwrap span{ font-size:${theme.fontsize.extraExtraLargeFont};}
                        .slidderwrap,
                        .toparticlewrap{
                            width:930px;
                            margin:0 auto;
                        }
                        .articlemaincontainer{
                            background-color: #f9f5f2;
                            color:${theme.colors.black};
                        }
                        .shortwrapper{ 
                            width:800px;
                            margin:0 auto;
                            // padding:70px 0; 
                        }
                        .bigwrapper{
                            width:100%;
                        }
                        .cardheading{
                            font-size:${theme.fontsize.semiBigFont};
                            font-weight:${theme.fontWeight.bold};
                            margin-bottom:40px;
                        }
                        .cardsubheading{ font-size:${theme.fontsize.largestFont}; }
                        .slidderholder{ 
                            width:90%;
                            margin-right: -16px;
                         }
                        .sliderbtwbutton,
                        .btnaroundslider{
                            display:flex;
                            justify-content:space-between;
                            align-items:center;
                            position:relative;
                        }
                        .absPrevBtn,
                        .absNextBtn{
                            position:absolute;
                            z-index:3;
                        }
                        .absPrevBtn{left:29%;}
                        .absNextBtn{right:32%}
                        .experinceslidder{
                            width:100%;
                        }
                        .descriptioncontainer{ 
                            width:800px;
                            margin:0 auto;
                            display:flex;
                            justify-content:space-between;
                        }
                        .descriptionwrapper{
                            width:90%;
                            margin:0 5%;
                            padding:5% 0;
                        }
                        
                    
                    @media screen and (min-width:1280px){
                        .slidderwrap,
                        .toparticlewrap{ width:910px; }
                        .shortwrapper{ width:800px; }
                        // .absPrevBtn{left:33%;}
                        // .absNextBtn{right:40%}
                    }

                    @media screen and (min-width:1366px){
                        
                    }

                    @media screen and (min-width:1680px){
                        .slidderwrap,
                        .toparticlewrap{ width:1150px }
                        .shortwrapper{ width:1000px}
                    }
                `}</style>
                <style global jsx>{`
                    @media screen and (min-width:960px){
                        .experinceslidder .slick-slide{
                            width:6.6% !important;
                        }
                        .experinceslidder .slick-track{
                            transform: translate3d(${this.state.x}%, 0, 0) !important;
                            transition: transform 1s;
                        }
                        .experinceslidder .slick-slide.slick-active.slick-current .carouselImage{
                            filter: none;
                            width:100%;
                            height:100%;
                        }
                        .experinceslidder .slick-slide.slick-cloned .carouselImage{
                            filter:none;
                            width:100%;
                            height:100%;
                        }
                        .experinceslidder .slick-slide .carouselImage{
                            filter: blur(5px);    
                        }
                    }
                `}</style>
            </div>

        )
    }
}