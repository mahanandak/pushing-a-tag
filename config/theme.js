const theme = {
    
    fontWeight: {
      light: 300,
      regular: 400,
      semiBold: 600,
      bold: 700,
      semiextrabold: 800,
      extrabold: 900
    },
    colors: {
      white: "#fff",
      halfwhite: "#f6f6f6",
      pinkwhite: "#F9F5F2",
      black: "#000",
      highlightcolor: "#d9233c",
      darkgray: "#999",
      lightgray: "#c9c9c9",
      overlay: "#00000033",
    },

    fontsize:{
      extrasmallestFont: '0.625em',   //10px
      exsmallFont:'0.6875em', //11px
      smallestFont: '0.75em', //12px;
      smallerFont: '0.875em', //14px
      smallFont: '0.9375em', //15px
      normalFont: '1em',    //16px
      mediumFont: '1.125em', //18px
      largeFont: '1.25em',   //20px
      largerFont: '1.3em',
      titleFont: '1.375em', //22px
      largestFont: '1.5em', //24px
      extraLargeFont: '1.6em',
      extraExtraLargeFont: '1.875em', //30px
      semiBigFont: '2.1875em',  //35px,
      bigFont: '3.125em', //50px
    },
  }
  
  export default theme