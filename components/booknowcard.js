import React from 'react';
import theme from '../config/theme';
import CustomImageWithoutLazyLoad from './common/customimagewithoutlazylaod';


export default React.memo(function BookNowCard(props){
  return (
      <a href="#" className="container">
          <div className="imgholder">
            <CustomImageWithoutLazyLoad image={props.img.url} alt={props.img.alt} href={props.img.href} mobilewidth={270}
              mobileheight={200} tabletwidth={295} tabletheight={500} desktopwidth={320} desktopheight={500} largedesktopwidth={380} 
              largedesktopheight={800}/>
          </div>
          <div className="description">
            <h3>{props.title}</h3>
            <div>
                <span className="BookingTitle">Book Now</span>
                <img className="arrowholder" src="https://images.travelxp.com/images/txpin/vector/general/long-arrow-right-red.svg" alt="right arrow"/> 
            </div>
          </div>

          <style jsx>{`
              .container{
                margin-right:16px;
              }
              .imgholder{
                  width:270px;
                  height:200px;
                  position:relative;
              }
              .description{
                width:100%;
                position:relative; 
                background-color: ${theme.colors.white};
                padding:1em 0;
              } 
              h3{
                  font-size:${theme.fontsize.largerFont};
                  font-weight: ${theme.fontWeight.bold};
                  color: ${theme.colors.black};
                  margin-bottom:1em;
                  text-align:center;
              }
              .description div{
                display:flex;
                justify-content:center;
                align-items:center;
              }
              .BookingTitle{
                  font-size: ${theme.fontsize.smallerFont};
                  font-weight: ${theme.fontWeight.bold};
                  // margin-bottom: 1em;
                  color:red;
                  margin-right:10px;
              }
              .arrowholder{
                  display:inline-block;
                  width:25px;
                  height:10px;
                  padding:1px;
              }
              @media screen and (min-width:960px){
                .container{
                    border-radius:12px;
                    box-shadow: rgba(0, 0, 0, 0.063) 0px 7px 10px 0px;
                    padding:8px;
                    overflow:hidden;
                    background-color:white;
                }
                .imgholder{
                    width:100%;
                    height:150px;
                    border-radius:12px;
                    overflow:hidden;
                }
                .description{
                  padding:0;                  
                }
                .description div{margin-bottom:0.5em;}
                h3{
                  margin:0.5em 0;
                }
              }
          `}</style>
      </a>
  )
})
