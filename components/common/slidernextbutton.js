import theme from '../../config/theme'
import sitesettings from '../../config/sitesettings'

export default React.memo(function SliderNextButton(props) {
    const { onNext, title, mobileshow, nextdisable } = props;
    return (
        <div className="nextprevbtn">
            <a className={nextdisable ? "button noevent" : 'button'} onClick={() => onNext(title)}>
                {nextdisable ?
                    <img src={sitesettings.svgimageUrl + '/general/rightarrowgray.svg'} />
                    :
                    <img src={sitesettings.svgimageUrl + '/general/long-arrow-next.svg'} />
                }
            </a>

            <style jsx>{`
                .noevent { pointer-events: none; }
                .button {
                    background-color: ${theme.colors.white};
                    width: 42px;
                    height: 42px;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    border-radius: 50%;
                    border: 1px solid ${theme.colors.lightgray}; 
                    box-shadow: 0 15px 25px 0 ${theme.colors.lightblackshadow1};
                }
                .button img {
                    width: 45%;
                }
                .button:hover img{
                    opacity: 0.7;
                }
                @media screen and (max-width: 959px) {
                    .nextprevbtn { display: ${mobileshow === "none" ? "none" : "inline-flex"} }
                    .button {
                        width: 37px;
                        height: 37px;
                    }
                    .button:hover img{
                        opacity: 1;
                    }
                }
            `}</style>
        </div>
    )
})