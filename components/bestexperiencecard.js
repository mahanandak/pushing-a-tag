import React from 'react';
import CustomImageWithoutLazyLoad from "../components/common/customimagewithoutlazylaod"
export default React.memo(function BestExperience(props){
    return(
        <div className="maincontainer">
            <div className="carouselImage">
                <div className="CenterImage">
                    <div className="RoundImage mainimage">
                        <CustomImageWithoutLazyLoad image={props.images.img1.url} alt={props.images.img1.alt} mobilewidth={165}
                        mobileheight={165} tabletwidth={250} tabletheight={250} desktopwidth={300} desktopheight={300} largedesktopwidth={400} 
                        largedesktopheight={400} />
                    </div>
                    <div className="titlecontainer">{props.title}</div>
                    <div className="TopImage">
                        <div className="RoundImage sideimage">
                            <CustomImageWithoutLazyLoad image={props.images.img2.url} alt={props.images.img2.alt} mobilewidth={270}
                            mobileheight={164} tabletwidth={250} tabletheight={300} desktopwidth={259} desktopheight={300} largedesktopwidth={399} 
                            largedesktopheight={460} />
                        </div>
                    </div>
                    <div className="BottomImage">
                        <div className="RoundImage sideimage">
                            <CustomImageWithoutLazyLoad image={props.images.img3.url} alt={props.images.img3.alt} mobilewidth={270}
                            mobileheight={164} tabletwidth={250} tabletheight={300} desktopwidth={259} desktopheight={300} largedesktopwidth={399} 
                            largedesktopheight={460} />
                        </div>
                    </div>
                </div>
                
            </div>
            
            <style jsx>{`
                .carouselImage,
                .maincontainer{
                    position:relative;    
                }
                .RoundImage{
                    border-radius: 100%;
                    overflow: hidden;
                }
                .sideimage{
                  width:100%;
                  height:100%;  
                }
                .CenterImage,
                .TopImage,
                .BottomImage{
                    border-radius: 100%;
                    border: 2px dashed #80808075;
                    background-color: white;
                }
                .CenterImage{ 
                    position: relative;
                    padding:10px; 
                }
                .TopImage{
                    height:30%;
                    width:30%;
                    position:absolute;
                    top:7px;
                    left:0px;
                    padding:5px;
                }
                .BottomImage{
                    padding:5px;
                    height:30%;
                    width:30%;
                    position:absolute;
                    bottom:7px;
                    right:0px
                }
                .titlecontainer{
                    background-color: white;
                    border-radius: 100%;
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%,-50%);
                    width: 50%;
                    height: 50%;
                    text-align: center;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                }

                @media screen and (min-width:960px){
                    .maincontainer{
                        display:flex;
                        justify-content:center;
                        margin: 0 10%;
                    }
                    .carouselImage{
                        width:80%;
                        height:80%;
                    }
                    .TopImage{
                        top: 10px;
                        left: -30px;
                    }
                    .BottomImage{
                        bottom:30px;
                        right:-30px;
                    }
                }
            `}</style>    
        </div>
    )
})