import React from 'react';
import theme from '../config/theme';
import CustomImage from './common/customimage';


export default React.memo(function MapCard(props){
  return (
      <a href="#" className="container">
          <div className="imgholder">
            <CustomImage image={props.img.url} alt={props.img.alt} href={props.img.href} mobilewidth={270}
              mobileheight={164} tabletwidth={295} tabletheight={500} desktopwidth={320} desktopheight={500} largedesktopwidth={380} 
              largedesktopheight={800}/>
          </div>
          <div className="description">
                <span className="placename">{props.title}</span>
                <img className="arrowholder" src="https://images.travelxp.com/images/txpin/vector/general/nextarrow.svg" alt="Right Arrow" /> 
          </div>

          <style jsx>{`
              .container{
                margin-bottom:20px;
                border-radius:20px;
                overflow:hidden;
                display:flex;
                box-shadow: rgba(0, 0, 0, 0.063) 0px 10px 20px 0px;
                background-color: ${theme.colors.white};
              }
              .imgholder{
                  width:35%;
                  height:100px;
              }
              .description{
                width:65%; 
                display: flex;
                align-items: center;
                padding: 0 12px;
                justify-content: space-between;
              } 
              .placename{
                  display:flex;
                  justify-content:space-between;
                  margin-right:10px;
                  font-size: ${theme.fontsize.smallerFont};
                  font-weight: ${theme.fontWeight.semiBold};
                  color: ${theme.colors.black};
              }
              .arrowholder{
                  display:inline-block;
                  width:25px;
                  height:10px;
              }
              @media screen and (min-width:960px){
               
              }
          `}</style>
      </a>
  )
})
