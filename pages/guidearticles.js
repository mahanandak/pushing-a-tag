import PageHeader from "../components/common/pageheader";
import theme from "../config/theme";
import sitesettings from "../config/sitesettings";
import CustomImage from "../components/common/customimage";
import Carousel from "../components/carousel1";

export default class index extends React.PureComponent {
    render() {
        const articledata = sitesettings.articlepage;
        console.log(articledata)
        return (
            <React.Fragment>
                <div className='banner'>
                    <PageHeader headerClass='nobg absolute' showsearchbox="true"/>
                    <h1>{articledata.banner.title}</h1>
                    <div className='background_image'>
                        <CustomImage image={articledata.banner.img.url} />
                    </div>
                </div>
                <div className='wrapper'>
                    {articledata.items.map((item, index) =>
                        <article>
                            <Carousel key={index} {...item} withlazy={true}/>
                        </article>
                    )}
                </div>
                <style jsx>{`
                    .banner {
                        position: relative;
                    }
                    h1 {
                      width: 100%;
                      text-align: center;
                      color: ${theme.colors.white};
                      font-size : ${theme.fontsize.largestFont};
                      font-weight: ${theme.fontWeight.bold};
                      position: absolute;
                      bottom: 34px;
                    }
                    .background_image {
                        height: 160px;
                    }
                    h1 { position: absolute;}
                    .wrapper {
                        margin-top: 30px;
                        display: flex;
                        flex-wrap: wrap;
                    }
                    article {
                        margin-bottom: 24px;
                    }
                    @media screen and (min-width: 960px) {
                        .wrapper {
                            width: 800px;
                        }
                        .background_image{
                            height:220px;
                        }
                        article {
                            width: 32%;
                            margin-right: 2%;
                        }
                        article:nth-child(3n) {
                            margin-right: 0;
                        }
                    }
                `}</style>
            </React.Fragment>
        )
    }
}