import React from 'react';
import theme from '../config/theme';
import CustomImageWithoutLazyLaod from './common/customimagewithoutlazylaod';

export default React.memo(function HolidayPackageCard(props){
  return (
      <article className="container">
          <div className="imgholder">
            <CustomImageWithoutLazyLaod image={props.img.url} alt={props.img.alt} href={props.img.href}  mobilewidth={312}
                        mobileheight={250} tabletwidth={295} tabletheight={500} desktopwidth={320} desktopheight={500} largedesktopwidth={380} largedesktopheight={800}/>
            <div className="overlay"></div>
          </div>
          <div className="description">
            <h3>{props.title}</h3>
            <p>{props.description}</p>
            <div>
                <span>Rs. {props.amount.toLocaleString()}</span>
                <span className="perperson"> / Person</span>
            </div>
          </div>

          <style jsx>{`
              .container{
                margin-right:16px;
              }
              .imgholder{
                  width:100%;
                  height:250px;
                  position:relative;
              }
              .overlay{
                height: 100%;
                width: 100%;
                position: absolute;
                top:0;
                background: ${theme.colors.overlay};
              }
              .description{
                position:relative; 
                background-color: ${theme.colors.white};
                padding:5% 7%;
              } 
              h3,p,.description span:firstchild, .perperson{
                font-family: 'Montserrat',sans-serif;    
              }  
              h3{
                  font-size:${theme.fontsize.mediumFont};
                  font-weight: ${theme.fontWeight.bold};
                  color: ${theme.colors.black};
                  margin-bottom:0.7em;
              }
              p{
                  font-size: ${theme.fontsize.smallerFont};
                  font-weight: ${theme.fontWeight.light};
                  margin-bottom: 1em;
                  color:${theme.colors.darkgray};
              }
              .description span:firstchild{
                font-size: ${theme.fontsize.normalFont};
                font-weight: ${theme.fontWeight.regular};
              }
              .perperson{
                font-weight: ${theme.fontWeight.light};
                font-size: ${theme.fontsize.normalFont};
              }
              @media screen and (min-width:960px){
                .imgholder{
                    border-radius:12px;
                    overflow:hidden;
                }
                .description{
                  width:80%;
                  position:relative;
                  left:10%;
                  bottom:45px;
                  border-radius:12px;
                }  
                .imgholder, .description{
                    box-shadow: rgba(0, 0, 0, 0.063) 5px 4px 4px 1px;
                }
                h3{
                    font-size:${theme.fontsize.largestFont};
                    font-weight:${theme.fontWeight.semiBold};
                    margin-bottom:0.7em;
                }
                p,span{font-size:${theme.fontsize.normalFont};}
              }
          `}</style>
      </article>
  )
})
