
import theme from '../config/theme'
import CustomImageWithouLazyLoad from './common/customimagewithoutlazylaod'
import Link from 'next/link'

export default React.memo(function BlogHorizontal(props) {
    return (
        <article>
            <Link href={props.img.href}>
                <a>
                    <span className="number mobile">{'0' + props.index+'.'}</span>
                    <div className='articlewrap'>
                        <div className="imagewrap">
                            {/* <img src="https://images.travelxp.com/images/egypt/egypt/camelcaravan.jpg" /> */}
                            <CustomImageWithouLazyLoad image={props.img.url} alt={props.img.alt} mobilewidth={270}
                                mobileheight={164} tabletwidth={230} tabletheight={596} desktopwidth={320} desktopheight={596} largedesktopwidth={380} largedesktopheight={800} />
                        </div>
                        <div className='content'>
                            <span className="number desktop">{'0' + props.index}</span>
                            <span className="status">{props.attribute}</span>
                            <h3>{props.title}</h3>
                            <p>
                                {props.description}
                            </p>
                        </div>
                    </div>
                </a>
            </Link>
            <style jsx>{`
                article {margin-right: 16px;}
                span { display: block; }
                a { color: ${theme.colors.black}; }
                .number{
                    margin-bottom: 8px;
                    font-size: ${theme.fontsize.semiBigFont};
                    font-weight: ${theme.fontWeight.bold};
                    color: ${theme.colors.lightgray};
                }
                img {
                    width: 100%;
                }
                .desktop { display: none; }
                .status {
                    color: ${theme.colors.highlightcolor};
                    font-size: ${theme.fontsize.smallestFont};
                    font-weight: ${theme.fontWeight.bold};
                    margin-bottom: 7px;
                }
                h3 {
                    font-family : 'Montserrat', sans-serif;
                    font-size: ${theme.fontsize.normalFont};
                    margin-bottom: 5px;
                }
                p {
                    font-size: ${theme.fontsize.smallerFont};
                    font-weight: ${theme.fontWeight.light};
                    color: ${theme.colors.darkgray};
                    line-height: 1.9;
                }
                .imagewrap {
                    margin-bottom: 10px;
                    height: 164px;
                }
                @media screen and (min-width: 960px) {
                    .mobile { display: none; }
                    .desktop { display: block; }
                    .articlewrap {
                        display: flex;
                    }
                    .imagewrap{
                        margin-right: 7%;
                        height: 420px;
                        width: 35%;
                    }
                    .content {
                        width: 52%;
                    }
                    .number {
                        margin-bottom: 35px;
                        margin-top:80px;
                    }
                    h3 {
                        font-size: ${theme.fontsize.largerFont};
                        margin-bottom: 15px;
                    }
                    p {
                        font-size: ${theme.fontsize.normalFont};
                        line-height: 1.5;
                    }
                }
            `}</style>
        </article>
    )
})