import React from 'react';
import theme from '../config/theme';
import CustomImageWithoutLazyLoad from './common/customimagewithoutlazylaod';
import CustomImage from '../components/common/customimage';
import Link from "next/link";

export default React.memo(function Carousel1(props) {
  const {showmobilesilder} = props;
  return (
    <Link href='/article'>
      <a className={ showmobilesilder === "true" ? "slidemainholder" : "mainholder"}>
        <div className={ showmobilesilder === "true" ? "slideimgholder" : "imgholder" }>
          {props.withlazy ?
            <CustomImage image={props.img.url} alt={props.img.alt} mobilewidth={270}
              mobileheight={164} tabletwidth={250} tabletheight={300} desktopwidth={259} desktopheight={300} largedesktopwidth={399} largedesktopheight={460} />
            :
            <CustomImageWithoutLazyLoad image={props.img.url} alt={props.img.alt} mobilewidth={270}
              mobileheight={164} tabletwidth={250} tabletheight={300} desktopwidth={259} desktopheight={300} largedesktopwidth={399} largedesktopheight={460} />
          }
        </div>
        <div className={ showmobilesilder === "true" ? "slideoverlay" : "overlay"}></div>
        <div className={ showmobilesilder === "true" ? "slidedescription": "description"}>
          <h3>{props.title}</h3>
          <div className="cardmiddle">
            <div className="authorname">
              <img src={props.author.avatar} alt={props.author.caption} />
              <span>{props.author.name}</span>
            </div>
            <div className={ showmobilesilder === "true" ? "slidesubhead":"subhead"}>
              <span>{props.date}</span>
              <span className="category">
                <span className="dot">&#8226;</span>
                <span>{props.category[0]}</span>
              </span>
            </div>
          </div>
          <p>Himachal Pradesh is a northern Indian state in the Himalayas....</p>
        </div>
        <style jsx>{`
              .mainholder{
                width:100%;
                display:flex;
                justify-content:flex-start;
              }  
              .slidemainholder{
                width:calc(100% - 16px);
                position:relative;
                padding:0;
                margin-right:16px;
              } 
              .imgholder{ width: 100px; }
              .slideimgholder{ 
                width:100%;
                height:200px;
              }
              .slideoverlay{
                height: 100%;
                width: 100%;
                position: absolute;
                top:0;
                background-image: linear-gradient(transparent, #00000059);
              }
              .description{
                margin-left: 16px;
                width: calc(100% - 116px)
              }
              .slidedescription{
                position:absolute;
                bottom:10px;
                box-sizing:border-box;
                width:100%;
                padding:0 7%;
                color:white;
              }
              h3,
              .slidedescription h3{
                margin-bottom: 4px;
                font-size: ${theme.fontsize.normalFont};
                line-height: 1.4;
              }
              .cardmiddle {
                margin-bottom: 6px;
              }
              .subhead,
              .slidesubhead{
                width:100%;
                font-size: ${theme.fontsize.exsmallFont};
                display:flex;
                justify-content: flex-start;
                align-items: center;
                white-space: nowrap;
              }
              .subhead{ color:${theme.colors.darkgray}; }
              .show{display:none;}
              p{
                text-align:justify;
                color:${theme.colors.darkgray};
                font-size: ${theme.fontsize.smallerFont};
                overflow: hidden;
                text-overflow: ellipsis;
                display: -webkit-box;
                line-height: 20px;
                -webkit-line-clamp: 2;
                -webkit-box-orient: vertical;
                max-height: 40px;
              }
              .slidedescription p{ display:none; }
              a { color: ${theme.colors.black}; }
              .category { margin-left: 3%; }
              .dot { margin-right: 10%; }

              .authorname { 
                display: flex;
                align-items: center;
                margin-bottom: 5px;
              }
              img{
                border-radius:50%; 
                width:15px;
                height:15px;
              }
              .authorname span{
                margin-left: 5px;
                font-size: ${theme.fontsize.smallestFont};
              }
              
              @media screen and (min-width:960px){
                .authorname { color: ${theme.colors.white}; }
                .mainholder,
                .slidemainholder{
                  // height:300px;
                  position:relative;
                  padding:0;
                }
                .mainholder{ height:300px;}
                .slidemainholder{ 
                  width:calc(100% - 16px);
                }
                .imgholder{ width:100% }
                .slideimgholder{ 
                  width:100%;
                  height:300px;
                }
                .overlay,
                .slideoverlay{
                  height: 100%;
                  width: 100%;
                  position: absolute;
                  background-image: linear-gradient(transparent, #00000059);
                }
                .description,
                .slidedescription{
                  position:absolute;
                  bottom:10px;
                  box-sizing:border-box;
                  width:100%;
                  padding:0 7%;
                }
                .slidedescription{
                  font-size:120%;
                }
                h3{
                  color:white;
                }
                .subhead,
                .slidesubhead{ color: ${theme.colors.white}; } 
                p{display:none;}
              }
              @media screen and (min-width:1680px){
                .mainholder { height: 400px; }
              }
              @media screen and (min-width:1920px){
                .mainholder { height: 460px; }
              }
          `}</style>
      </a>
    </Link>
  )
})
