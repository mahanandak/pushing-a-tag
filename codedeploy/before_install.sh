# #!/bin/bash

# # This script is executed before copying the source

apt-get update

curl --silent --location https://rpm.nodesource.com/setup_4.x | bash -
apt install nodejs

npm install -g pm2
pm2 update

export app_root=/var/www/backup/poc1/travelxp
if [ -d "$app_root" ];then
    sudo rm -rf /var/www/backup/poc1/travelxp
    sudo mkdir -p /var/www/backup/poc1/travelxp
else
    sudo mkdir -p /var/www/backup/poc1/travelxp
fi


