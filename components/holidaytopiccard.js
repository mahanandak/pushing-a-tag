import React from 'react';
import theme from '../config/theme';
import CustomImageWithoutLazyLoad from './common/customimagewithoutlazylaod';


export default React.memo(function HolidayTopicCard(props){
  return (
      <article className="container">
          <div className="imgholder">
          <CustomImageWithoutLazyLoad image={props.img.url} alt={props.img.alt} href={props.img.href} mobilewidth={270}
                        mobileheight={164} tabletheight={300} desktopwidth={786} desktopheight={270} largedesktopwidth={399} largedesktopheight={460}/>
          </div>
          <div className="description">
            <h3>{props.title}</h3>
          </div>

          <style jsx>{`
              .container{ margin-right:16px; }
              .imgholder{
                  width:100%;
                  height:250px;
                  box-shadow: rgba(0,0,0,0.063) 4px 4px 3px 3px;
              }
              .description{
                position:relative; 
                padding:5% 7%;
                width:80%;
                left:10%;
                bottom:27px;
                background-color: ${theme.colors.white};
                box-shadow: rgba(0, 0, 0, 0.063) 4px 4px 3px 3px;
              }  
              h3{
                  font-size:${theme.fontsize.smallerFont};
                  font-weight: ${theme.fontWeight.bold};
                  color: ${theme.colors.black};
                  text-align:center;
              }
              
              @media screen and (min-width:680px){
                .imgholder{
                  height:270px;
                }
              }

              @media screen and (min-width:960px){
                .imgholder{
                    height:300px;
                }
                .description{
                    bottom:22px;
                }
                h3{
                  font-size:${theme.fontsize.mediumFont};
                }
                
              }
          `}</style>
      </article>
  )
})
