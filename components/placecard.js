import React from 'react';
import theme from '../config/theme';
import CustomImage from './common/customimage';


export default React.memo(function PlaceCard(props){
  return (
      <article className="container">
          <div className="imgholder">
            <CustomImage image={props.img.url} alt={props.img.alt} href={props.img.href}  mobilewidth={270}
                        mobileheight={164} tabletwidth={295} tabletheight={500} desktopwidth={320} desktopheight={500} largedesktopwidth={380} 
                        largedesktopheight={800}/>
          </div>
          <div className="description">
            <h3>{props.title}</h3>
          </div>

          <style jsx>{`
              .container{
                display:inline-block;
                width:49%;
                margin:0 2% 10px 0;
                box-shadow: rgba(0, 0, 0, 0.063) 4px 4px 3px 3px;
              }
              .container:nth-child(even){
                margin-right:0;
              }
              .imgholder{
                  width:100%;
                  height:130px;
              }
              
              .description{
                padding:5% 7%;
                background-color: ${theme.colors.white};
                box-shadow: rgba(0, 0, 0, 0.063) 0px 10px 20px 0px;
              }  
              h3{
                  font-size:${theme.fontsize.smallerFont};
                  font-weight: ${theme.fontWeight.bold};
                  color: ${theme.colors.black};
                  text-align:center;
              }
              
              @media screen and (min-width:960px){
                .container{
                    width:23%;                }
                .imgholder{
                  height:200px;
                }
                .container,
                .container:nth-child(even){
                  margin:0 2.6% 30px 0;
                }
                .container:nth-child(4n){
                  margin-right:0;
                }
                h3{ font-size:${theme.fontsize.mediumFont};}
              }
          `}</style>
      </article>
  )
})
