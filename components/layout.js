import theme from '../config/theme'
import PageFooter from '../components/common/pagefooter'

function Layout(props) {
    return (
      <React.Fragment>
          {props.children}
          <PageFooter/>
        <style jsx global>{`
          * { box-sizing: border-box; }
          body {
            margin: 0;
            color: ${theme.colors.black};
            font-family: 'Lato', sans-serif;
            font-size: 1em;
            background-color: ${theme.colors.backgroundcolor};
            -webkit-overflow-scrolling : touch;
          }
          html {
            -webkit-overflow-scrolling : touch;
          }

          h1, h2, h3, h4, h5, h6 {
            font-family: 'Montserrat', sans-serif;
            margin: 0;
          }
          a { 
            cursor: pointer; 
            display: inline-block;
            text-decoration: none;
          }
          button {
            cursor: pointer; 
            outline: none;
            }
          ul { 
            padding: 0; 
            margin: 0;
          }
          p{ margin: 0; }
          li { list-style-type: none;}
          
          .wrapper {
            width: 92%;
            margin: 0 auto;
          }

          .modal {
            transition: all 0.2s ease-in-out;
            background-color: #fff;
            height: 100vh;
            width: 100%;
            position: fixed;
            right: 0;
            left: 0;
            top: 0;
            z-index:2;
            transition: all 0.2s ease-in-out;
          }

          .modal-show { 
            top: 0; 
            overflow: scroll;
            visibility: visible;
          }
          .modal-hide { 
            top: 100%; 
            visibility: hidden;
          }
          input {
            -webkit-appearance: none;
          }
          
          input[type=number]::-webkit-inner-spin-button, 
          input[type=number]::-webkit-outer-spin-button { 
            -webkit-appearance: none; 
            margin: 0; 
          }

          input:-webkit-autofill,
          input:-webkit-autofill:hover,
          input:-webkit-autofill:focus,
          input:-webkit-autofill:active {
              transition: background-color 5000s ease-in-out 0s;
          }

          .slick-list .slick-track {
            height: auto !important;
          }

          //slick slide..........

          .slick-dots li button:before{  }

          .hottopic .slick-dots li.slick-active button:before {
            color: ${theme.colors.highlightcolor};
            opacity: 1;
          }
          .slick-dots li.slick-active button:before {
            color: ${theme.colors.black};
            opacity: 1;
          }
          .hottopic .slick-dots li button:before {
            color: ${theme.colors.lightgray};
            font-size: 12px;
          }
          .slick-dots li button:before {
            color: ${theme.colors.darkgray};
            font-size: 8px;
          }

          .slickmargin .slick-dots {
            bottom: 0;
          }
          @media screen and (min-width: 960px) {
            
            .modal {
              width: 420px;
            }
            .modal-show {
              top: 0;
              right: 0px;
              left: auto;
              transition: all 0.5s;
              overflow: auto;
            }
            .modal-hide {
              top: 0;
              right: -425px;
              left: auto;
              display: block; 
              transition: all 0.5s;
            }
          }
        `}</style>
      </React.Fragment>
    )
  }
  
  export default Layout