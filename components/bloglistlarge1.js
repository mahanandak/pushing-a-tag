
import theme from '../config/theme';
import CustomImageWithoutLazyLoad from './common/customimagewithoutlazylaod';

export default React.memo(function BloglistlargeCard(props){
  return (
      <article className="container">
        <CustomImageWithoutLazyLoad image={props.img.url} alt={props.img.alt} href={props.img.href} mobilewidth={270}
                        mobileheight={164} tabletwidth={295} tabletheight={596} desktopwidth={320} desktopheight={596} largedesktopwidth={380} largedesktopheight={800}/>
        <div className="overlay"></div>
        <div className="description">
          <h3>{props.title}</h3>
          <p>{props.caption}</p>
        </div>

          <style jsx>{`
              .container{
                position:relative;
                color:white;
                margin:0 6px;
              }
              .overlay{
                height: 100%;
                width: 100%;
                position: absolute;
                top:0;
                background-image: linear-gradient(transparent, #000000a3);
              }
              .description{
                position:absolute;
                bottom:25px;
                left:5%;
                right:5%;
              }   
              h3{
                  font-size:${theme.fontsize.normalFont};
                  font-weight: ${theme.fontWeight.extrabold};
                  font-family: 'Montserrat',sans-serif;
                  margin-bottom: 0.5em;
              }
              p{
                  font-size: ${theme.fontsize.smallestFont};
                  font-weight: ${theme.fontWeight.regular};
                  line-height:1.5;
              }
              
              @media screen and (min-width:960px){
                .container{
                  height:230px;
                }
                h3 {
                  font-size: ${theme.fontsize.largeFont};
                  font-weight: ${theme.fontWeight.bold};
                }
                p{
                  font-size: ${theme.fontsize.smallerFont};
                  font-weight: ${theme.fontWeight.light};
                }
              }
              @media screen and (min-width:1280px){
                .container { height: 275px; }
              }
              @media screen and (min-width:1680px){
                .container { height: 350px; }
              }
              @media screen and (min-width:1920px){
                .container { height: 410px; }
              }
          `}</style>
      </article>
  )
})
