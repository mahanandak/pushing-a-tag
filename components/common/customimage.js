import React from 'react'
import PropTypes from 'prop-types';

 export default class CustomImage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentImage: '../../static/images/thumbnail.png',
    }
    this.ref = React.createRef();
  }


  async componentDidMount() {
    if(!('IntersectionObserver' in window)){
      await import('intersection-observer')
    }

    const imageObserver = this.imageObserver();

    if (this.ref.current) {
      imageObserver.observe(this.ref.current)
    }
  }

  imageObserver(){
    const returndata = new IntersectionObserver(([entry]) => {
      if (entry.intersectionRatio > 0) {
        this.setImage(entry)
      }
    },
      {
        root: null,
        rootMargin: '0px',
        threshold: [1.0]
      });
      return returndata
  }

  setImage(entry) {
    const { image } = this.props;

    let imagesrc;
    if (typeof (image) === "string") {
      imagesrc = image.indexOf('images.travelxp.com') !== -1 ? image + '?tr=w-' + entry.target.width + ',h-' + entry.target.height : image;
    }
    else if (typeof (image) === "object") {
      imagesrc = ""
    }
    else {
      imagesrc = image
    }
    this.setState({
      currentImage: imagesrc
    },()=>this.ref.current)
  }

  render() {
    return (
      <React.Fragment>
        <img ref={this.ref} src={this.state.currentImage} className="lzy_img" />
        <style jsx>{`
          img {
            width: 100%;
            height: 100%;
            object-fit: cover;
          }
        `}</style>
      </React.Fragment>
    )
  }
}


CustomImage.propTypes = {
  image: PropTypes.string.isRequired
};