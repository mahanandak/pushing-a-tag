import React from 'react';
import dynamic from 'next/dynamic';
import sitesettings from '../config/sitesettings';
import theme from '../config/theme';
import Slider from 'react-slick';
const PageHeader = dynamic(import("../components/common/pageheader"));
const PackageCard = dynamic(import("../components/packagecard"));
const BookNowCard = dynamic(import("../components/booknowcard"));
const MapCard = dynamic(import("../components/mapcard"));
const Carousel1 = dynamic(import("../components/carousel1"));
const ActivityCard = dynamic(import("../components/activitycard"));
const SliderButtons = dynamic(import("../components/common/sliderbuttons"));
const GoButton = dynamic(import("../components/common/gobutton"));
const article = sitesettings.trendingarticle;

export default class Article extends React.PureComponent{
    constructor(){
        super();
        this.state = {
            packageindex:0,
            carouselindex:0
        }
        this.myDivSection = React.createRef()
    }
    scrollDown() {
        console.log("clicked");
        if (this.myDivSection.current) {
          let toPos = this.myDivSection.current.offsetTop;
          console.log(toPos);
          window.scrollTo(0, toPos - 80)
        }
    }

    previous = (type) =>{
        if(type === "packagecard"){
            this.packagecard.slickPrev();}
        else if (type === 'carousel') {
            this.carousel.slickPrev();
        }
    }

    next = (type) =>{
        if(type === "packagecard"){
            this.packagecard.slickNext();
        }
        else if (type === 'carousel') {
            this.carousel.slickNext();
        }
    }

    carouselchange = (e, name) => {
        console.log(e)
        this.setState({ [name]: e })
    }

    render(){
        console.log("data :",sitesettings.trendingarticle);
        const holidayPackage = {
            afterChange: (e) => this.carouselchange(e, 'packageindex'),
            responsive: [
                {
                  breakpoint: 959,
                  settings: {
                    slidesToShow: 1.1,
                    slidesToScroll: 1,
                    infinite: false,
                    ladyLoad: true,
                    arrows: false
                  }
                },
                {
                    breakpoint: 9999,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 1,
                      infinite: false,
                      ladyLoad: true,
                      arrows: false
                    }
                  }
            ]
        }
        const carousel1 = {
            arrows: false,
            afterChange: (e) => this.carouselchange(e, 'carouselindex'),
            responsive: [
              {
                breakpoint: 9999,
                settings: {
                  infinite: false,
                  speed: 500,
                  slidesToShow: 2,
                  slidesToScroll: 1,
                  lazyLoad: true,
                  arrows: false,
                }
              },
              {
                breakpoint: 1365,
                settings: {
                  infinite: false,
                  slidesToShow: 2,
                  slidesToScroll: 1,
                  arrows: false,
                }
              },
              {
                breakpoint: 1279,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1,
                }
              },
              {
                breakpoint: 959,
                settings: {
                  infinite: false,
                  vertical: true,
                  slidesToShow: article !== null ? article.collection[5].items.length : 4,
                  dots: false,
                }
              }
            ]
          }
        return(
            <div className="article">
                <PageHeader showsearchbox="true" searchwithborder="true" />
                <main>
                    {/*------------------------banner-----------------------------------*/}
                    <div className="banner">
                        <div className="bannerheading wrapper">
                            <h1>{article.banner.name}</h1>
                            <div className="authordetail">
                                <div className="authorinfo">
                                    <img src={article.articleinfo.author.avatar} alt="Avatar"/>
                                    <div className="authorname">
                                        <span className="authorby">By</span> 
                                        &nbsp;<span>{article.articleinfo.author.name}</span>
                                    </div>
                                    <span className="dot">&middot;</span>
                                    <span>{article.articleinfo.articledate}</span>
                                </div>
                                <img src={sitesettings.svgimageUrl + "/general/bookmark.svg"}/>
                            </div>
                        </div>
                        <div className="bannercontent">
                            <img src='./static/images/web-blog-details.svg' alt="Banner Image" className="bannerImage"/>
                            <div className="desktopbanner">
                                <div className="bannerhead wrapper">
                                <h1>{article.banner.name}</h1>
                                    <div className="authordetail">
                                        <div className="authorinfo">
                                            <img src={article.articleinfo.author.avatar} alt="Avatar"/>
                                            <div className="authorname">
                                                <span className="authorby">By</span> 
                                                &nbsp;<span>{article.articleinfo.author.name}</span>
                                            </div>
                                            <span className="dot">&middot;</span>
                                            <span>{article.articleinfo.articledate}</span>
                                        </div>
                                        <img src={sitesettings.svgimageUrl + "/general/bookmark.svg"}/>
                                    </div>
                                </div>
                                <div>
                                    {article.banner.items.map((item,i)=>
                                        <a className="bannerlinks" href={item.href} key={i}>{item.title}</a>
                                    )}
                                </div>
                                <a className="scrolldown" onClick={() => this.scrollDown()} title="Scroll Down">
                                    <img src={sitesettings.svgimageUrl + "/general/arrowdownblack.svg"} alt="click to scroll" />
                                </a>
                            </div>
                        </div>
                    </div>
                    {/*------------------------------------------banner-------------------------------*/}

                    {article.articles.map((item, index) =>
                        <React.Fragment key={index}>
                        <article className="wrapper bestplacearticle">
                            <h2>{item.title}</h2>
                            <div dangerouslySetInnerHTML={{ __html: item.htmlcontent }} ></div>
                        </article>
                        {article.collection.filter((filteritem) => {
                            return filteritem.after === index + 1
                            }).map((childitem, index) =>
                                <section key={index} id={childitem.type}>
                                        
                                    {childitem.type === "packagecard" ?
                                        <div className="maincontainer" ref={this.myDivSection}>
                                        <div className="packagecontainer slidderwrap">
                                            <h2 className="packageHeader">{childitem.name}</h2>
                                            <div className="leftbtnwrap">
                                                <h2 className="lefttopic">{childitem.name}</h2>
                                                <SliderButtons title="packagecard" onPrevious={this.previous} onNext={this.next} prevdisable={this.state.packageindex <= 0} nextdisable={this.state.packageindex === childitem.items.length - 2} mobileshow="none"/>
                                            </div>
                                            <div className="packageslider">
                                                <Slider ref={c => (this.packagecard = c)} {...holidayPackage}>
                                                    {childitem.items.map((childitem, childindex) =>
                                                        <PackageCard {...childitem} key={childindex + "c"} index={childindex + 1}/>
                                                    )}
                                                </Slider>
                                            </div>
                                            <div className="centerbtnwrap">
                                                <SliderButtons title="packagecard" onPrevious={this.previous} onNext={this.next} prevdisable={this.state.packageindex <= 0} nextdisable={this.state.packageindex === childitem.items.length - 1}/>
                                            </div>
                                        </div>
                                        </div>
                                    
                                    :childitem.type === "booknowcard" ?
                                    <div className="maincontainer">
                                        <div className="cardcontainer slidderwrap">
                                            <h2 className="topicCardHeader">{childitem.name}</h2>
                                            <div className="sliderAuto">
                                                {childitem.items.map((childitem, childindex) =>
                                                    <BookNowCard {...childitem} key={childindex + "c"} index={childindex + 1}/>
                                                )}
                                            </div>
                                        </div>
                                    </div>

                                    : childitem.type === "mapcard" ?
                                    <div className="mapmaincontainer">
                                        <div className="mapcardcontainer bigwrapper">
                                            <div className="mapcontainer">
                                                <h2 className="topicCardHeader">{childitem.name}</h2>
                                                <div className="sectiondivision">
                                                    {childitem.items.map((childitem, childindex) =>
                                                            <MapCard {...childitem} key={childindex + "c"} index={childindex + 1}/>
                                                    )}    
                                                </div>
                                                <div className="gobutton">
                                                    <GoButton title="View All Places"/>
                                                </div>
                                            </div>
                                            <div className="map"></div>
                                        </div>
                                    </div>

                                    : childitem.type === "activitycard" ?
                                    <div className="activityMainContainer">
                                        <div className="cardcontainer bigwrapper">
                                            <h2 className="topicCardHeader">{childitem.name}</h2>
                                            <p>{childitem.subtitle}</p>
                                            <div className="multiactivity">
                                                {childitem.items.map((childitem, childindex)=>
                                                    <ActivityCard {...childitem} key={childindex+"c"} index={childindex+1} />
                                                )}
                                            </div>
                                            <div className="nextbutton">
                                                <GoButton title="Next"/>
                                            </div>
                                        </div>
                                    </div>

                                    : childitem.type === "articlevideo" ?
                                    <div>
                                        <div className="videoholder">
                                            <div className="bigwrapper">
                                                <video poster={childitem.video.thumbnail} controls>
                                                    <source src={childitem.video.sources[0]} type="video/mp4"></source>
                                                </video>
                                                {/* <img src={childitem.video.thumbnail} alt={childitem.video.title} width="100%" height="200px"/> */}
                                                <div dangerouslySetInnerHTML={{ __html: childitem.video.description }} className="videodescription"></div>
                                            </div>
                                        </div>
                                        <div className="sociallinkholder">
                                            {childitem.social.map((socialitem,socialindex)=>
                                                <React.Fragment key={socialindex}>
                                                {socialitem.type === "facebook" ?
                                                    <a href={socialitem.href} target="_blank" className="socialicon">
                                                        <img src={socialitem.img} alt="Facebook Icon" />
                                                    </a>
                                                 : socialitem.type === "twitter" ?
                                                 <a href={socialitem.href} target="_blank" className="socialicon">
                                                     <img src={socialitem.img} alt="Facebook Icon" />
                                                 </a>
                                                 : socialitem.type === "pinterest" &&
                                                 <a href={socialitem.href} target="_blank" className="socialicon">
                                                     <img src={socialitem.img} alt="Facebook Icon" />
                                                 </a>
                                                }
                                                </React.Fragment>
                                            )}

                                        </div>
                                    </div>

                                    : childitem.type === 'carousel1' &&
                                    <div className="carouselmaincontainer">
                                        <div className="carouselcontainer carouselwrap">
                                            <h2 className="packageHeader">{childitem.name}</h2>
                                            <div className="desktopleftcontainer">
                                                <div>
                                                    <h2 className="CardHeader">{childitem.name}</h2>
                                                    <SliderButtons title="carousel" onPrevious={this.previous} onNext={this.next} prevdisable={this.state.carouselindex <= 0} nextdisable={this.state.carouselindex === childitem.items.length - 2} mobileshow="none"/>
                                                </div>
                                                <div><GoButton title="View All"></GoButton></div>
                                            </div>

                                            <div className="trendingpicksslider">
                                                <Slider ref={c => (this.carousel = c)} {...carousel1}>
                                                {childitem.items.map((childitem, childindex) =>
                                                    <article key={childindex} className="trendingarticle">
                                                    <Carousel1
                                                        {...childitem} key={childindex + "c"} index={childindex + 1}/>
                                                    </article>
                                                )}
                                                </Slider>
                                            </div>
                                        </div>
                                    </div>
                                    }
                                </section>
                            )}

                        </React.Fragment>
                    )}
                </main>
                <style jsx>{`
                    .wrapper,
                    .bigwrapper,
                    .activitywrap,
                    .carouselwrap{
                        width:94%;
                        margin:0 auto;
                    }
                    .slidderwrap{
                        width:96%;
                        margin-left:4%;
                    }
                    .bannerhead,
                    .carouselbtn{display:none;}
                    .banner{
                        width:100%;
                        margin:25px 0 75px;
                    }
                    .bannerheading{
                        margin-bottom:50px;
                    }
                    .bannerheading h1{
                        font-family: 'Montserrat',sans-serif;
                        font-size:${theme.fontsize.mediumFont};
                        font-weight:${theme.fontWeight.extrabold};
                        line-height:1.5;
                    }
                    .authordetail{
                        display:flex;
                        justify-content:space-between;
                        align-items:center;
                        margin-top:20px;
                    }
                    .authorinfo{
                        display:flex;
                        justify-content:space-around;
                        align-items:center;
                        font-size:${theme.fontsize.smallerFont};
                        font-family: 'latto',sans-serif;
                        color:${theme.colors.darkgray};
                    }
                    .authorname{margin-left:10px;}
                    .authorby{color:${theme.colors.black}}
                    .dot{
                        font-size:2em;
                        margin:0 10px;
                    }
                    .authorinfo img{
                        width:30px;
                        height:30px;
                        border-radius:50%;
                    }
                    .bannercontent{
                        display:flex;
                        justify-content:space-between;
                        width:96%;
                        margin-right:4%;
                    }
                    .bannerImage{width:60%;}
                    .bannerlinks{
                        display:inline-block;
                        border:1px solid grey;
                        border-radius: 30px;
                        padding:5%;
                        margin:0 10px 10px 0;
                        color:${theme.colors.darkgray};
                        font-size:${theme.fontsize.smallestFont};
                        font-family:'latto',sans-serif;
                    }
                    .scrolldown{display:none;}
                    .maincontainer,
                    .mapmaincontainer{
                        background-color:#f9f5f2;
                        margin:70px 0;
                    }
                    .activityMainContainer{
                        background-color:#f2fcfc;
                        margin:70px 0;
                    }
                    .packagecontainer,
                    .carouselconatiner,
                    .mapcontainer
                    {padding:40px 0}
                    .cardcontainer{padding:70px 0;}
                    .cardcontainer p
                    { 
                        text-align:center;
                        margin-bottom:40px;
                        font-family: 'Montserrat',sans-serif;
                    }
                    .topicCardHeader,
                    .packageHeader,
                    .carouselhead
                    {
                        margin-bottom:40px;
                        text-align:center;
                        font-size:${theme.fontsize.largeFont};
                        font-weight:${theme.fontWeight.extrabold};
                        line-height:1.5;
                        font-family:'Montserrat',sans-serif;
                    }
                    .cardHeader{
                        font-size:${theme.fontsize.largeFont};
                        font-weight:${theme.fontWeight.extrabold};
                        line-height:1.5;
                        font-family:'Montserrat',sans-serif;
                    }
                    .packageHeader,
                    .carouselhead{margin-right:3%;}
                    .leftbtnwrap,.map{display:none;}
                    .sliderAuto{
                        display:flex;
                        overflow: auto;
                    }
                    ::-webkit-scrollbar {
                        width: 0px;  
                        background: transparent;  
                    }
                    .centerbtnwrap{
                        text-align:center;
                        margin-top:30px;
                    }
                    .gobutton,
                    .nextbutton{ text-align:center; }
                    .trendingpicksslider{ padding-bottom:15px;}
                    .multiactivity{ margin:20px 0; }
                    .desktopleftcontainer{ display:none; }
                    .trendingarticle{
                        margin-bottom:20px;
                    }
                    .videoholder{
                        background-color:${theme.colors.black};
                        padding:70px 0;
                    }
                    .carouselmaincontainer{ margin:70px 0 10px; }
                    video{width:100%;}
                    .sociallinkholder{
                        display: flex;
                        justify-content: center;
                        padding: 40px 0;
                        border-bottom: 3px dashed #80808085;
                        width:94%;
                        margin:0 auto;
                    }
                    .socialicon{ margin-right:20px; }
                    .sociallinkholder a:last-child{ margin-right:0;}
                    }

                    @media screen and (min-width:680px){
                    }

                    @media screen and (min-width:960px){
                        .activitywrapper{
                            width:900px;
                        }
                        .bigwrapper{width:930px}
                        .mapcardcontainer, .carouselcontainer,
                        .packagecontainer{
                            display:flex;
                            justify-content:space-between;
                            padding:70px 0;
                        }
                        .bannerheading{display:none;}
                        .desktopbanner{
                            margin-left:70px;
                            position:relative;}
                        .bannerlinks{
                            margin:0px 20px 20px 0;
                            border-radius:50px;
                            padding:20px;
                            font-size:${theme.fontsize.smallFont};
                        }
                        .bannerhead{
                            display:block;
                            margin:70px 0px;
                        }
                        .banner{
                            width:90%;
                            margin: 0 auto 100px 0;
                        }
                        .bannercontent{
                            width:100%;
                            margin-right:0;
                        }
                        .bannerhead h1{
                            line-height:1.2;
                        }
                        .authorinfo img{
                            width:50px;
                            height:50px;
                        }
                        .scrolldown{
                            display:inline-block;
                            width:20px;
                            position:absolute;
                            bottom:0;
                        }
                        .slidderwrap,
                        .bestplacearticle,
                        .carouselwrap{
                            width:850px;
                            margin: 0 auto;
                        }
                        .packageslider,
                        .trendingpicksslider{
                            width:calc(100% - 300px);
                        }
                        .packageHeader,
                        .centerbtnwrap,
                        .carouselhead{display:none}
                        .leftbtnwrap{
                            display:block;
                            width:300px;
                        }
                        .lefttopic,.lefthead{
                            font-size:${theme.fontsize.largestFont};
                            font-family:'Montserrat',sans-serif;
                            font-weight:${theme.fontWeight.semiextrabold};
                            line-height:1.5;
                        }
                        .lefttopic{margin-bottom:100px}
                        .lefthead{margin-bottom:30px}
                        .topicCardHeader{
                            font-size:${theme.fontsize.largestFont};
                        }
                        .mapmaincontainer{
                            display:flex;
                            justify-content:space-between;
                        }
                        .mapcontainer{
                            width:300px;
                            text-align:left;
                            padding:0;
                        }
                        .mapcontainer .topicCardHeader,.gobutton
                        {text-align:left;}
                        .sectiondivision{
                            margin:40px 0;
                        }
                        .multiactivity{margin: 70px 0 40px;}
                        .map{
                            width:calc(100% - 300px);
                        }
                        .bigwrapper .topicCardHeader{
                            margin-bottom:10px;
                        }
                        .sliderAuto{
                            overflow:unset;
                        }
                        .carouselbtn{
                            display:block;
                            margin-top:100px;
                        }
                        .mobcarouselbtn{display:none}
                        .trendingarticle{
                            width:95% !important;
                            margin-left:10px;
                        }
                        .desktopleftcontainer{
                            display:flex;
                            flex-direction:column;
                            justify-content:space-around;
                            width:300px;
                        }
                        .desktopleftcontainer h2{
                            margin-bottom:25px;
                        }
                        .nextbutton{text-align:center;color:red}
                        .videoholder{ background-color:${theme.colors.white}}
                        .videodescription{ margin: 0 13%; }
                        .sociallinkholder{ 
                            width:60%;
                            padding-top:0;
                        }
                    }
                    @media screen and (min-width:1280px){
                        .banner{width:1200px;}
                        .slidderwrap,
                        .bestplacearticle,
                        .carouselwrap{width:900px;}
                        .mapcontainer{width:30%;}
                        .bigwrapper{width:1180px;}
                    }
                    @media screen and (min-width:1366px){
                        .banner{width:1300px}
                    }
                    @media screen and (min-width:1680px){
                        .banner{width:1400px}
                    }
                    @media screen and (min-width:1920px){
                        .banner{width:1500px}
                    }
                `}</style>
                <style global jsx>{`
                    .bestplacearticle img{
                        width: 100%;
                    }
                    .bestplacearticle h2{
                        margin:80px 0 30px;
                        font-weight:${theme.fontWeight.semiextrabold};
                    }
                    .bestplacearticle p{
                        margin-bottom:30px;
                        line-height:2;
                        font-size:${theme.fontsize.smallerFont};
                        font-family:'latto',sans-serif;
                        color:${theme.colors.darkgray};
                    }
                    .bestplacearticle img{
                        margin: 10px 0 30px;
                        width:100%;
                    }
                    .videodescription p{
                        color:${theme.colors.white};
                        margin-top:30px;
                        line-height:2;
                        font-size:${theme.fontsize.smallerFont};
                        font-family:'latto',sans-serif;
                    }

                    @media screen and (min-width:960px){
                        .bestplacearticle h2{
                            font-size:${theme.fontsize.extraLargeFont};
                        }
                        .bestplacearticle p{
                            font-size:${theme.fontsize.normalFont};
                        }
                        .videodescription p{
                            font-size:${theme.fontsize.normalFont};
                            color:${theme.colors.darkgray};
                            margin-top:40px;
                        }
                    }
                `}</style>
            </div>
        )
    }
}