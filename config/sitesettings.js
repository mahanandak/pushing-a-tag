const sitesettings = {
  svgimageUrl: "https://images.travelxp.com/images/txpin/vector",
  serviceHeader: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'dev': "1",
    'said': '1'
  },
  serviceUrl: 'https://api.travelxp.guide',
  timeFormatHM: time => {
    var timeHM = time.split(":");
    return timeHM[0] + "h " + timeHM[1] + "m"
  },
  titleCase(str) {

    str = str.toLowerCase().split(' ');

    let final = [];

    for (let word of str) {
      final.push(word.charAt(0).toUpperCase() + word.slice(1));
    }

    return final.join(' ')

  },
  getSeo(seo) {
    // console.log(seo);
    const DEFAULT_SEO = {
      title: seo.title,
      description: seo.meta_description !== undefined ? seo.meta_description : '',
      // noindex: true,

      facebook: {
        appId: seo.fb_appid !== undefined ? seo.fb_appid : ''
      },

      twitter: {
        handle: seo.tw_creator !== undefined ? seo.tw_creator : '',
        site: seo.tw_creator !== undefined ? seo.tw_creator : '',
        // cardType: undefined
      },

      openGraph: {
        type: 'website',
        url: seo.fb_url !== undefined ? seo.fb_url : '',
        title: seo.fb_title !== undefined ? seo.fb_title : '',
        description: seo.fb_desc !== undefined ? seo.fb_desc : '',
        images: seo.fb_image !== undefined ? [{ url: seo.fb_image }] : [],
      },

      additionalMetaTags: [{
        name: "keywords",
        content: seo.meta_keywords
      }]
    }
    return DEFAULT_SEO;
  },

  sliderimages: [
    "https://images.travelxp.com/deals/dealflighthome/mumbaiworlisealink.jpg",
    "https://images.travelxp.com/deals/dealflighthome/jodhpurumaidbhawanpalace.jpg",
    "https://images.travelxp.com/deals/dealhotelhome/dubaiexpo.jpg?tr=w-1366,h-527"
  ],

  trendingarticle: {
    "banner": {
      "name": "10 Best Places To Visit In Malappuram To Satiate Your Wanderlust While In Kerala!",
      "items": [
        {
          "title": "Holidays in Kerala",
          "href": "/"
        },
        {
          "title": "Holidays in Kerala",
          "href": "/"
        },
        {
          "title": "Holidays in Kerala",
          "href": "/"
        },
        {
          "title": "Holidays in Kerala",
          "href": "/"
        },
        {
          "title": "Holidays in Kerala",
          "href": "/"
        }
      ]
    },
    "articleinfo": {
      "author": {
        "name": "Prashant Chothani",
        "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4",
        "bio": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae sem dolor. Vestibulum molestie pretium sem in faucibus. Curabitur ornare lorem a nulla euismod accumsan. Suspendisse auctor rutrum gravida"
      },
      "articledate": "14-06-2019"
    },
    "articles": [
      {
        "title": "Thirumandhamkunnu Temple",
        "htmlcontent": "<p>Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod. Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><img src='https://images.travelxp.com/images/mauritius/mauritius/mauritiusbeachview.jpg?tr=w-1293,h-628'/><p>\nNam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><p>Maecenas sit amet purus eget ipsum elementum venenatis. Aenean maximus urna magna, quis rutrum mi semper non. Cras rhoncus elit non arcu hendrerit rhoncus.</p>"
      },
      {
        "title": "Kadalundi Bird Sanctuary",
        "htmlcontent": "<p>Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod. Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><img src='https://images.travelxp.com/images/mauritius/mauritius/mauritiusbeachview.jpg?tr=w-1293,h-628'/><p>\nNam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><p>Maecenas sit amet purus eget ipsum elementum venenatis. Aenean maximus urna magna, quis rutrum mi semper non. Cras rhoncus elit non arcu hendrerit rhoncus.</p>"
      },
      {
        "title": "Pazhayangadi Mosque",
        "htmlcontent": "<p>Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod. Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><img src='https://images.travelxp.com/images/mauritius/mauritius/mauritiusbeachview.jpg?tr=w-1293,h-628'/><p>\nNam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><p>Maecenas sit amet purus eget ipsum elementum venenatis. Aenean maximus urna magna, quis rutrum mi semper non. Cras rhoncus elit non arcu hendrerit rhoncus.</p>"
      },
      {
        "title": "Keralmkundu Waterfalls",
        "htmlcontent": "<p>Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod. Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><img src='https://images.travelxp.com/images/mauritius/mauritius/mauritiusbeachview.jpg?tr=w-1293,h-628'/><p>\nNam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><p>Maecenas sit amet purus eget ipsum elementum venenatis. Aenean maximus urna magna, quis rutrum mi semper non. Cras rhoncus elit non arcu hendrerit rhoncus.</p>"
      },
      {
        "title": "Kodikuthimala",
        "htmlcontent": "<p>Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod. Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><img src='https://images.travelxp.com/images/mauritius/mauritius/mauritiusbeachview.jpg?tr=w-1293,h-628'/><p>\nNam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><p>Maecenas sit amet purus eget ipsum elementum venenatis. Aenean maximus urna magna, quis rutrum mi semper non. Cras rhoncus elit non arcu hendrerit rhoncus.</p>"
      },
      {
        "title": "Padinharekera Beach",
        "htmlcontent": "<p>Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod. Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><img src='https://images.travelxp.com/images/mauritius/mauritius/mauritiusbeachview.jpg?tr=w-1293,h-628'/><p>\nNam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><p>Maecenas sit amet purus eget ipsum elementum venenatis. Aenean maximus urna magna, quis rutrum mi semper non. Cras rhoncus elit non arcu hendrerit rhoncus.</p>"
      },
      {
        "title": "Kodikuthimala",
        "htmlcontent": "<p>Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod. Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><img src='https://images.travelxp.com/images/mauritius/mauritius/mauritiusbeachview.jpg?tr=w-1293,h-628'/><p>\nNam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><p>Maecenas sit amet purus eget ipsum elementum venenatis. Aenean maximus urna magna, quis rutrum mi semper non. Cras rhoncus elit non arcu hendrerit rhoncus.</p>"
      },
      {
        "title": "Padinharekera Beach",
        "htmlcontent": "<p>Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod. Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><img src='https://images.travelxp.com/images/mauritius/mauritius/mauritiusbeachview.jpg?tr=w-1293,h-628'/><p>\nNam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.Nam ut rutrum ex, venenatis sollicitudin urna. Aliquam erat volutpat. Integer eu ipsum sem. Ut bibendum lacus vestibulum maximus suscipit. Quisque vitae nibh iaculis neque blandit euismod.</p><p>Maecenas sit amet purus eget ipsum elementum venenatis. Aenean maximus urna magna, quis rutrum mi semper non. Cras rhoncus elit non arcu hendrerit rhoncus.</p>"
      }
    ],
    "collection": [
      {
        "type": "packagecard",
        "name": "80+ Keral Holiday Packages On TravelTriangle",
        "items": [
          {
            "title": "Kerala",
            "description": "5 days  in May from Mumbai",
            "amount": 69999,
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "Kerala",
            "description": "7 days  in May from Mumbai",
            "amount": 109999,
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "Kerala",
            "description": "4 days  in May from Mumbai",
            "amount": 49999,
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          }
        ],
        "after": 2
      },
      {
        "type": "booknowcard",
        "name": "Best Time to Visit Kerala",
        "items": [
          {
            "title": "January",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "February",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "April",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "November",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "December",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          }
        ],
        "after": 4
      },
      {
        "type": "mapcard",
        "name": "Kerala Map",
        "items": [
          {
            "title": "Thirumandhamkunnu Temple",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "Kadalundu Bird Sanctuary",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "Pazhayangadi Mosque",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "Kerlamkundu Water falls",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "Alapi Boat Ride",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          }
        ],
        "after": 6
      },
      {
        "type": "activitycard",
        "name": "What would you like to do in Kerala?",
        "subtitle": "you can select Multiple Options",
        "after": 7,
        "items": [
          {
            "title": "Beaches",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "Houseboat",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "Nature",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "Forest",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "Wild Life",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "Traditional Food",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "Beaches",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "Nature",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          },
          {
            "title": "Houseboat",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          }
        ]
      },
      {
        "type": "articlevideo",
        "after": 8,
        "video": {
          "description": "<p>Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge.\n\nLicensed under the Creative Commons Attribution license\nhttp://www.bigbuckbunny.org</p><p>Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge.\n\nLicensed under the Creative Commons Attribution license\nhttp://www.bigbuckbunny.org</p>",
          "sources": [
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
          ],
          "subtitle": "By Blender Foundation",
          "title": "Big Buck Bunny",
          "thumbnail": "https://images.travelxp.com/images/dubai/hattadamdubai.jpg"
        },
        "social": [
          {
            "type": "facebook",
            "href": "https://www.facebook.com/travelxpcom/",
            "img": "https://images.travelxp.com/images/txpin/vector/general/facebookcircle.svg"
          },
          {
            "type": "twitter",
            "href": "https://www.twitter.com",
            "img": "https://images.travelxp.com/images/txpin/vector/general/twittercircle.svg"
          },
          {
            "type": "pinterest",
            "href": "https://www.pinterest.com",
            "img": "https://images.travelxp.com/images/txpin/vector/general/pinterestcircle.svg"
          }
        ]
      },
      {
        "type": "carousel1",
        "name": "Related Top Articles",
        "after": 8,
        "items": [
          {
            "title": "Your guide to the perfect summer vacation",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Maldives",
              "href": "/"
            },
            "category": [
              "Beaches"
            ],
            "date": "28-11-2019",
            "author": {
              "name": "Prashant Chothani",
              "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
            }
          },
          {
            "title": "Your guide to the perfect summer vacation",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Maldives",
              "href": "/"
            },
            "category": [
              "Beaches"
            ],
            "date": "28-11-2019",
            "author": {
              "name": "Prashant Chothani",
              "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
            }
          },
          {
            "title": "Your guide to the perfect summer vacation",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Maldives",
              "href": "/"
            },
            "category": [
              "Nature"
            ],
            "date": "28-11-2019",
            "author": {
              "name": "Prashant Chothani",
              "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
            }
          },
          {
            "title": "Your guide to the perfect summer vacation",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Maldives",
              "href": "/"
            },
            "category": [
              "Beaches"
            ],
            "date": "28-11-2019",
            "author": {
              "name": "Prashant Chothani",
              "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
            }
          }
        ]
      }
    ]
  },

  ///////////////
  articlepage: {
    "banner": {
      "img": {
        "url": "https://images.travelxp.com/deals/dealhotelhome/americagoldengatebridge.jpg",
        "alt": "America"
      },
      "title": "Trending Picks Article"
    },
    "items": [
      {
        "title": "Your guide to the perfect summer vacation",
        "img": {
          "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
          "alt": "Maldives",
          "href": "/"
        },
        "category": ["Beaches"],
        "date": "28-11-2019",
        "author": {
          "name": "Prashant Chothani",
          "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
        }
      },
      {
        "title": "Your guide to the perfect summer vacation",
        "img": {
          "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
          "alt": "Maldives",
          "href": "/"
        },
        "category": ["Beaches"],
        "date": "28-11-2019",
        "author": {
          "name": "Prashant Chothani",
          "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
        }
      },
      {
        "title": "Your guide to the perfect summer vacation",
        "img": {
          "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
          "alt": "Maldives",
          "href": "/"
        },
        "category": ["Beaches"],
        "date": "28-11-2019",
        "author": {
          "name": "Prashant Chothani",
          "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
        }
      },
      {
        "title": "Your guide to the perfect summer vacation",
        "img": {
          "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
          "alt": "Maldives",
          "href": "/"
        },
        "category": ["Beaches"],
        "date": "28-11-2019",
        "author": {
          "name": "Prashant Chothani",
          "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
        }
      },
      {
        "title": "Your guide to the perfect summer vacation",
        "img": {
          "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
          "alt": "Maldives",
          "href": "/"
        },
        "category": ["Beaches"],
        "date": "28-11-2019",
        "author": {
          "name": "Prashant Chothani",
          "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
        }
      },
      {
        "title": "Your guide to the perfect summer vacation",
        "img": {
          "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
          "alt": "Maldives",
          "href": "/"
        },
        "category": ["Beaches"],
        "date": "28-11-2019",
        "author": {
          "name": "Prashant Chothani",
          "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
        }
      },
      {
        "title": "Your guide to the perfect summer vacation",
        "img": {
          "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
          "alt": "Maldives",
          "href": "/"
        },
        "category": ["Beaches"],
        "date": "28-11-2019",
        "author": {
          "name": "Prashant Chothani",
          "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
        }
      },
      {
        "title": "Your guide to the perfect summer vacation",
        "img": {
          "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
          "alt": "Maldives",
          "href": "/"
        },
        "category": ["Beaches"],
        "date": "28-11-2019",
        "author": {
          "name": "Prashant Chothani",
          "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
        }
      },
      {
        "title": "Your guide to the perfect summer vacation",
        "img": {
          "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
          "alt": "Maldives",
          "href": "/"
        },
        "category": ["Beaches"],
        "date": "28-11-2019",
        "author": {
          "name": "Prashant Chothani",
          "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
        }
      }
    ]
  }, 

  hotTopicPage:{
    "banner": {
      "img": {
        "url": "https://images.travelxp.com/images/txpin/holidays/525x280/pack_205.jpg",
        "alt": "HotTopic"
      },
      "title": "Category"
    },
    "items": [
      {
        "title": "Beaches",
        "img": {
          "url": "https://st2.travelxp.com/images/txpin/holidays/themes/beachxp_300x200.jpg",
          "alt": "Beaches",
          "href": "#"
        }
      },
      {
        "title": "Romance",
        "img": {
          "url": "https://st2.travelxp.com/images/txpin/holidays/themes/beachxp_300x200.jpg",
          "alt": "Romance",
          "href": "#"
        }
      },
      {
        "title": "NightLife",
        "img": {
          "url": "https://st2.travelxp.com/images/txpin/holidays/themes/beachxp_300x200.jpg",
          "alt": "Food and NightLife",
          "href": "#"
        }
      },
      {
        "title": "Art and Culture",
        "img": {
          "url": "https://st2.travelxp.com/images/txpin/holidays/themes/beachxp_300x200.jpg",
          "alt": "Art and Culture",
          "href": "#"
        }
      },
      {
        "title": "Mountain",
        "img": {
          "url": "https://st2.travelxp.com/images/txpin/holidays/themes/beachxp_300x200.jpg",
          "alt": "mountain",
          "href": "#"
        }
      },
      {
        "title": "Summer",
        "img": {
          "url": "https://st2.travelxp.com/images/txpin/holidays/themes/beachxp_300x200.jpg",
          "alt": "summer",
          "href": "#"
        }
      },
      {
        "title": "NightLife",
        "img": {
          "url": "https://st2.travelxp.com/images/txpin/holidays/themes/beachxp_300x200.jpg",
          "alt": "Food and NightLife",
          "href": "#"
        }
      },
      {
        "title": "Art and Culture",
        "img": {
          "url": "https://st2.travelxp.com/images/txpin/holidays/themes/beachxp_300x200.jpg",
          "alt": "Art and Culture",
          "href": "#"
        }
      }
    ]
  },

  articlecategory:{
    "banner": {
      "name": "Kerala",
      "subtitle": "Discover",
      "description": "Himachal Pradesh is a northern Indian State in the Himalayas.",
      "img":"https://images.travelxp.com/images/indonesia/bali/coastallandscapeatwaterBlowbaliindonesia.jpg"
    },
    "collection":[
      {
        "type":"bestexperiencecard",
        "name":"Best Experience in Kerala",
        "subtitle":"Experience",
        "items":[
          {
            "title":"HouseBoat",
            "images":
              {"img1":
                {
                  "url":"https://images.travelxp.com/images/bali/tanahlottemple.jpg",
                  "alt":"img1"
                },
                "img2":
                {
                  "url":"https://images.travelxp.com/images/greece/greecegenericview.jpg",
                  "alt":"img2"
                },
                "img3":
                {
                  "url":"https://images.travelxp.com/images/dubai/atlantisthepalm.jpg",
                  "alt":"img3"
                }
              },
            "information":{
              "video":
              {
                "sources": [
                    "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
                ],
                "thumbnail": "https://images.travelxp.com/images/dubai/hattadamdubai.jpg"
              },
              "description":"Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge."
            }
          },
          {
            "title":"Forest",
            "images":
              {"img1":
                {
                  "url":"https://images.travelxp.com/images/bali/tanahlottemple.jpg",
                  "alt":"img1"
                },
                "img2":
                {
                  "url":"https://images.travelxp.com/images/greece/greecegenericview.jpg",
                  "alt":"img2"
                },
                "img3":
                {
                  "url":"https://images.travelxp.com/images/dubai/atlantisthepalm.jpg",
                  "alt":"img3"
                }
              },
            "information":{
              "video":
              {
                "sources": [
                    "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
                ],
                "thumbnail": "https://images.travelxp.com/images/dubai/hattadamdubai.jpg"
              },
              "description":"Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. "
            }
          },
          {
            "title":"WildLife",
            "images":
              {"img1":
                {
                  "url":"https://images.travelxp.com/images/bali/tanahlottemple.jpg",
                  "alt":"img1"
                },
                "img2":
                {
                  "url":"https://images.travelxp.com/images/greece/greecegenericview.jpg",
                  "alt":"img2"
                },
                "img3":
                {
                  "url":"https://images.travelxp.com/images/dubai/atlantisthepalm.jpg",
                  "alt":"img3"
                }
              },
            "information":{
              "video":
              {
                "sources": [
                    "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
                ],
                "thumbnail": "https://images.travelxp.com/images/dubai/hattadamdubai.jpg"
              },
              "description":"Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge."
            }
          },
          {
            "title":"WaterSports",
            "images":
              {"img1":
                {
                  "url":"https://images.travelxp.com/images/bali/tanahlottemple.jpg",
                  "alt":"img1"
                },
                "img2":
                {
                  "url":"https://images.travelxp.com/images/greece/greecegenericview.jpg",
                  "alt":"img2"
                },
                "img3":
                {
                  "url":"https://images.travelxp.com/images/dubai/atlantisthepalm.jpg",
                  "alt":"img3"
                }
              },
            "information":{
              "video":
              {
                "sources": [
                    "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
                ],
                "thumbnail": "https://images.travelxp.com/images/dubai/hattadamdubai.jpg"
              },
              "description":"Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge."
            }
          },
          {
            "title":"Adventures",
            "images":
              {"img1":
                {
                  "url":"https://images.travelxp.com/images/bali/tanahlottemple.jpg",
                  "alt":"img1"
                },
                "img2":
                {
                  "url":"https://images.travelxp.com/images/greece/greecegenericview.jpg",
                  "alt":"img2"
                },
                "img3":
                {
                  "url":"https://images.travelxp.com/images/dubai/atlantisthepalm.jpg",
                  "alt":"img3"
                }
              },
            "information":{
              "video":
              {
                "sources": [
                    "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
                ],
                "thumbnail": "https://images.travelxp.com/images/dubai/hattadamdubai.jpg"
              },
              "description":"Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge."
            }
          },
        ]
      },
      {
        "type":"holidaytopiccard",
        "name":"Things to do in Kerala",
        "subtitle":"Top Things",
        "items":[
          {
            "title": "Holidays in Kerala",
            "img": {
              "url": "https://images.travelxp.com/images/txpin/holidays/themes/adventurexp_300x200.jpg",
              "alt": "Holidays in Kerla",
              "href": "/"
            }
          },
          {
            "title": "Holidays in Kerala",
            "img": {
              "url": "https://images.travelxp.com/images/txpin/holidays/themes/adventurexp_300x200.jpg",
              "alt": "Holidays in Kerla",
              "href": "/"
            }
          },
          {
            "title": "Holidays in Kerala",
            "img": {
              "url": "https://images.travelxp.com/images/txpin/holidays/themes/adventurexp_300x200.jpg",
              "alt": "Holidays in Kerla",
              "href": "/"
            }
          },
          {
            "title": "Holidays in Kerala",
            "img": {
              "url": "https://images.travelxp.com/images/txpin/holidays/themes/adventurexp_300x200.jpg",
              "alt": "Holidays in Kerla",
              "href": "/"
            }
          }  
          
        ]
      },
      {
        "type": "placecard",
        "name": "Top Destinations",
        "subtitle": "Places",
        "items": [
          {
            "title": "Munnar",
            "img": {
              "url": "https://images.travelxp.com/images/txpin/holidays/themes/adventurexp_300x200.jpg",
              "alt": "Munnar",
              "href": "/"
            }
          },
          {
            "title": "Cochin",
            "img": {
              "url": "https://images.travelxp.com/images/txpin/holidays/themes/adventurexp_300x200.jpg",
              "alt": "Cochin",
              "href": "/"
            }
          },
          {
            "title": "Wayanad",
            "img": {
              "url": "https://images.travelxp.com/images/txpin/holidays/themes/adventurexp_300x200.jpg",
              "alt": "Wayanad",
              "href": "/"
            }
          },
          {
            "title": "Kovalam",
            "img": {
              "url": "https://images.travelxp.com/images/txpin/holidays/themes/adventurexp_300x200.jpg",
              "alt": "Kovalam",
              "href": "/"
            }
          },
          {
            "title": "Kumarakom",
            "img": {
              "url": "https://images.travelxp.com/images/txpin/holidays/themes/adventurexp_300x200.jpg",
              "alt": "Kumarakom",
              "href": "/"
            }
          },
          {
            "title": "Alleppey",
            "img": {
              "url": "https://images.travelxp.com/images/txpin/holidays/themes/adventurexp_300x200.jpg",
              "alt": "Alleppey",
              "href": "/"
            }
          },
          {
            "title": "Kollam",
            "img": {
              "url": "https://images.travelxp.com/images/txpin/holidays/themes/adventurexp_300x200.jpg",
              "alt": "Kollam",
              "href": "/"
            }
          },
          {
            "title": "kovalam",
            "img": {
              "url": "https://images.travelxp.com/images/txpin/holidays/themes/adventurexp_300x200.jpg",
              "alt": "Kovalam",
              "href": "/"
            }
          },
          {
            "title": "Houseboat",
            "img": {
              "url": "https://images.travelxp.com/images/txpin/holidays/themes/adventurexp_300x200.jpg",
              "alt": "Kerala",
              "href": "/"
            }
          }
        ]
      },
      {
        "type": "carousel1",
        "name": "Top Articles",
        "subtitle":"Read Now",
        "items": [
          {
            "title": "Your guide to the perfect summer vacation",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Maldives",
              "href": "/"
            },
            "category": [
              "Beaches"
            ],
            "date": "28-11-2019",
            "author": {
              "name": "Prashant Chothani",
              "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
            }
          },
          {
            "title": "Your guide to the perfect summer vacation",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Maldives",
              "href": "/"
            },
            "category": [
              "Beaches"
            ],
            "date": "28-11-2019",
            "author": {
              "name": "Prashant Chothani",
              "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
            }
          },
          {
            "title": "Your guide to the perfect summer vacation",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Maldives",
              "href": "/"
            },
            "category": [
              "Nature"
            ],
            "date": "28-11-2019",
            "author": {
              "name": "Prashant Chothani",
              "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
            }
          },
          {
            "title": "Your guide to the perfect summer vacation",
            "img": {
              "url": "https://images.travelxp.com/images/maldives/maldives/maldivesview.jpg",
              "alt": "Maldives",
              "href": "/"
            },
            "category": [
              "Beaches"
            ],
            "date": "28-11-2019",
            "author": {
              "name": "Prashant Chothani",
              "avatar": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/p32x32/13221660_10206190701943282_322206579945385274_n.jpg?_nc_cat=110&_nc_ohc=-WmppBpd-eIAQkJoGEVAxeMd1aylnuPYDdA4hc5adHrhn3XYhtVsabkjg&_nc_ht=scontent-frx5-1.xx&oh=b0e7377255b5cd5fab8fd5aaa0751c2d&oe=5E702FB4"
            }
          }
        ]
      }
    ]
  },

  searchcities:{
    "cities": [
      {
        "img": {
          "url": "https://images.travelxp.com/images/bali/tanahlottemple.jpg",
          "alt": "cities"
        },
        "categories": "Kerala",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/greece/greecegenericview.jpg",
          "alt": "cities"
        },
        "categories": "Kerala Beaches",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/dubai/atlantisthepalm.jpg",
          "alt": "cities"
        },
        "categories": "Kerala Top Things",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/bali/tanahlottemple.jpg",
          "alt": "cities"
        },
        "categories": "Jaipur",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/greece/greecegenericview.jpg",
          "alt": "cities"
        },
        "categories": "Deradhun",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/dubai/atlantisthepalm.jpg",
          "alt": "cities"
        },
        "categories": "Jaipur Top Things",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/bali/tanahlottemple.jpg",
          "alt": "cities"
        },
        "categories": "Shimla",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/greece/greecegenericview.jpg",
          "alt": "cities"
        },
        "categories": "Goa Beaches",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/dubai/atlantisthepalm.jpg",
          "alt": "cities"
        },
        "categories": "Shimla Top Things",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/bali/tanahlottemple.jpg",
          "alt": "cities"
        },
        "categories": "Goa",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/greece/greecegenericview.jpg",
          "alt": "cities"
        },
        "categories": "Goa Watersports",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/dubai/atlantisthepalm.jpg",
          "alt": "cities"
        },
        "categories": "Deradhun Adventures",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/bali/tanahlottemple.jpg",
          "alt": "cities"
        },
        "categories": "Assam",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/greece/greecegenericview.jpg",
          "alt": "cities"
        },
        "categories": "Goa Markets",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/dubai/atlantisthepalm.jpg",
          "alt": "cities"
        },
        "categories": "Dubai",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/bali/tanahlottemple.jpg",
          "alt": "cities"
        },
        "categories": "Dubai Top Things",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/greece/greecegenericview.jpg",
          "alt": "cities"
        },
        "categories": "Bali",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/dubai/atlantisthepalm.jpg",
          "alt": "cities"
        },
        "categories": "Bali Top Things",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/bali/tanahlottemple.jpg",
          "alt": "cities"
        },
        "categories": "Shimla",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/greece/greecegenericview.jpg",
          "alt": "cities"
        },
        "categories": "Bali Beaches",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/dubai/atlantisthepalm.jpg",
          "alt": "cities"
        },
        "categories": "Madhya Pradesh Top Things",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/bali/tanahlottemple.jpg",
          "alt": "cities"
        },
        "categories": "Madhya Pradesh",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/greece/greecegenericview.jpg",
          "alt": "cities"
        },
        "categories": "Madhya Pradesh Monuments",
        "href": "/categories"
      },
      {
        "img": {
          "url": "https://images.travelxp.com/images/dubai/atlantisthepalm.jpg",
          "alt": "cities"
        },
        "categories": "Assam Hill Station",
        "href": "/categories"
      }
    ]
  }
    
}

export default sitesettings